//----dependencias------  
'use strict'
const nodemailer = require('nodemailer');
const plantilla = require('./plantillaCorreo');

function enviarCorreo(usuario, contrasenia, cliente, negocio) {

	//---- Configurar Cuenta ------  
	const transporter = nodemailer.createTransport({
	  service: 'gmail',
	  auth: {
	    user: 'dreamworks.promo57@gmail.com', // correo emisor
	    pass: 'uclaequipo2'                  // contraseña del correo
	  }
	});

	//---- Configurar Msj ------  
	/*const mensaje = "¡Hola, " + cliente.nombres + " " + cliente.apellidos + "!\n\n" +
										"Gracias por suscribirte a nuestro portal a través de Sunset. " +
										"Ahora puedes solicitar alguno de nuestros servicios descargando nuestra aplicación móvil en " +
										"https://enlace.de.descarga.com, " +
										"e iniciar sesión con el correo electrónico y contraseña que suministraste:\n\n" +
										"Correo: " + usuario.correo + "\n" +
										"Contraseña: " + contrasenia + "\n\n" +
										negocio.get('nombre') + ", tu salud, belleza y bienestar en manos de los mejores especialistas.\n\n" +
										"Powered by Sunset.";*/

	//---- Configurar Correo ------  
	const mailOptions = {
	  from: 	'dreamworks.promo57@gmail.com', //cuenta emisor
	  to: 		usuario.correo,     			//cuenta destino
	  subject:  '¡' + negocio.get('nombre') + ' te da la bienvenida!',     	//asunto msj
		//text: 	mensaje                     //texto msj
		html:		plantilla.cargarPlantilla(usuario, contrasenia, cliente, negocio)
	};

	//---- Enviar Correo  ------  
	transporter.sendMail(mailOptions, function(error, info){
	  if (error) {
	    console.log(error);
	  } else {
	    console.log('Email enviado: ' + info.response);
	  }
	});

}

module.exports = { enviarCorreo };

