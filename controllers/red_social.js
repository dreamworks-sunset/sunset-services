//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Red_social = require('../models/red_social');
const cloudinary = require('../cloudinary'); //Importar para poder usar


exports.findDocuments = (req,res) => {
  
  Red_social.forge().fetchAll({withRelated:['negocio','red_social_negocio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

    const logo = req.files.logo//Se crea una constante que recibe el objeto de la imagen
    cloudinary.uploader.upload(logo.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion

      if (result.error) {
        return res.status(500).json({
            error: true,
            data: { message: result.error }
          });
      } 

  let newData = {
    descripcion:          req.body.descripcion,
    logo:                 result.url,//La funcion anterior devuelve un resultado, y ese objeto se llama su atributo url y se obtiene la url donde fue guardada la imagen en cloudinary
  }

  Red_social.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'red_social creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });
   })

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Red_social.forge(conditions).fetch({withRelated:['negocio','red_social_negocio']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'red_social no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {
if(req.body.cambio_estatus != 'true'){

            
          
const logo = req.files.logo//Se crea una constante que recibe el objeto de la imagen
var resultado;
if (logo.originalFilename != ''){
    cloudinary.uploader.upload(logo.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion

      if (result.error) {
        return res.status(500).json({
            error: true,
            data: { message: result.error }
          });
      } 
      resultado = result;
    });
    //Fin imagen
  }

  let conditions = { id: req.params.id };

  Red_social.forge(conditions).fetch()
    .then(function(red_social){
      if(!red_social) return res.status(404).json({ error : true, data : { message : 'red_social no existe' } });
      var imagen = (resultado == undefined) ? red_social.logo : resultado.url;
      let updateData = {
        id:                   req.params.id,
        descripcion:          req.body.descripcion,
       logo:                 imagen,//La funcion anterior devuelve un resultado, y ese objeto se llama su atributo url y se obtiene la url donde fue guardada la imagen en cloudinary      
        estatus:              req.body.estatus
      }
      
      red_social.save(updateData)
        .then(function(data){

          res.status(200).json({ error : false, data : { message : 'red_social actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })
    
    } else
    {
let conditions = { id: req.params.id };

  Red_social.forge(conditions).fetch()
    .then(function(red_social){
      if(!red_social) return res.status(404).json({ error : true, data : { message : 'red_social no existe' } });

      let updateData = {
          estatus:              req.body.estatus
      }
      
      red_social.save(updateData)
        .then(function(data){

          res.status(200).json({ error : false, data : { message : 'red_social actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })
    }

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Red_social.forge(conditions).fetch()
    .then(function(red_social){
      if(!red_social) return res.status(404).json({ error : true, data : { message : 'red_social no existe' } });

      Red_social.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'red_social eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}