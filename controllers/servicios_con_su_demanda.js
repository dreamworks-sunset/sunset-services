//----dependencias------  
'use strict'
const Servicios_con_su_demanda = require('../models/servicios_con_su_demanda');

exports.findDocuments = (req,res) => {
    
  Servicios_con_su_demanda.forge().fetchAll()
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}