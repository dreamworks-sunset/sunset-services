//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Cita = require('../models/cita');

exports.findDocuments = (req,res) => {
  
  Cita.forge().fetchAll({withRelated:['orden_servicio' , 'sesion' , 'agenda', 'agenda.horario_empleado.empleado','orden_servicio.solicitud.servicio','orden_servicio.solicitud.cliente','orden_servicio.solicitud.promocion','orden_servicio.solicitud.tipo_solicitud']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

 
  console.log(req.body);
  let newData = {
    id_orden_servicio:   req.body.id_orden_servicio,
    id_empleado:                       req.body.id_empleado,
    fecha:                             req.body.fecha,

 
     }  

  Cita.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'cita creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Cita.forge(conditions).fetch({withRelated:['orden_servicio' , 'sesion' , 'agenda.horario_empleado.empleado', 'agenda','orden_servicio.solicitud.servicio','orden_servicio.solicitud.cliente','orden_servicio.solicitud.promocion','orden_servicio.solicitud.tipo_solicitud']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'cita no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Cita.forge(conditions).fetch()
    .then(function(cita){
      if(!cita) return res.status(404).json({ error : true, data : { message : 'cita no existe' } });

      let updateData = {
    id_cita:                           req.body.id_cita,
    id_horario:                        req.body.id_horario,
    id_empleado:                       req.body.id_empleado,
    estatus:                      req.body.estatus,
      }
      
      Cita.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'cita actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Cita.forge(conditions).fetch()
    .then(function(cita){
      if(!cita) return res.status(404).json({ error : true, data : { message : 'cita no existe' } });

     Cita.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'cita eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}