//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Empleado = require('../models/empleado');

exports.findDocuments = (req,res) => {
  
  Empleado.forge().fetchAll({withRelated:['solicitud','negocio','usuario','ciudad','horario','tipo_servicio', 'horario_empleado']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {
    nombres:         req.body.nombres,
    apellidos:    req.body.apellidos,
    cedula:        req.body.cedula,
    telefono:       req.body.telefono,
    direccion:       req.body.direccion,
    fecha_nacimiento:    req.body.fecha_nacimiento


  }

  Empleado.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'empleado creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Empleado.forge(conditions).fetch({withRelated:['solicitud','negocio','usuario','ciudad','horario','tipo_servicio' , 'horario_empleado']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'empleado no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Empleado.forge(conditions).fetch()
    .then(function(empleado){
      if(!empleado) return res.status(404).json({ error : true, data : { message : 'empleado no existe' } });

      let updateData = {
        id:                         req.params.id,
        nombres:                    req.body.nombres,
        apellidos:                  req.body.apellidos,
        cedula:                     req.body.cedula,
        telefono:                   req.body.telefono,
        direccion:                  req.body.direccion,
        fecha_nacimiento:           req.body.fecha_nacimiento,       
        estatus:                    req.body.estatus

      }
      
      empleado.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'empleado actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Empleado.forge(conditions).fetch()
    .then(function(empleado){
      if(!empleado) return res.status(404).json({ error : true, data : { message : 'empleado no existe' } });

      Empleado.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'empleado eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}