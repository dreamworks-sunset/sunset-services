//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Vista_reporte_estructurado_post_servicio = require('../models/vista_reporte_estructurado_post_servicio');

exports.findDocuments = (req,res) => {
    
  Vista_reporte_estructurado_post_servicio.forge().fetchAll()
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}
