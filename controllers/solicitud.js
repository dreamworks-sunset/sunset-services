//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Solicitud = require('../models/solicitud');

exports.findDocuments = (req,res) => {
  
  Solicitud.forge().fetchAll({withRelated:['bloque_hora', 'empleado','cliente','tipo_solicitud','tipo_motivo','promocion','servicio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findSolicitudesRealizadas = (req,res) => {
  
  Solicitud.forge().where('estatus', '=', 2).fetchAll({withRelated:['bloque_hora', 'empleado','cliente','tipo_solicitud','tipo_motivo','promocion','servicio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {
	console.log(req.body);
  let newData = {
    //Primera fase
    id_cliente:           req.body.id_cliente,
    id_servicio:          req.body.id_servicio,
    id_tipo_solicitud:    req.body.id_tipo_solicitud,
    //id_tipo_motivo:       req.body.id_tipo_motivo,
    //Fin primera fase
    id_promocion:         req.body.id_promocion,
    /*id_empleado:          req.body.id_empleado,
    id_horario:          req.body.id_horario,*/
  }

  Solicitud.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'solicitud creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Solicitud.forge(conditions).fetch({withRelated:['bloque_hora', 'empleado','cliente','tipo_solicitud','tipo_motivo','promocion','servicio']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'solicitud no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Solicitud.forge(conditions).fetch()
    .then(function(solicitud){
      if(!solicitud) return res.status(404).json({ error : true, data : { message : 'solicitud no existe' } });

      let updateData = {
        id:                    req.params.id,
        id_cliente:            req.body.id_cliente,
        id_servicio:           req.body.id_servicio,
        id_tipo_solicitud:     req.body.id_tipo_solicitud,
        id_tipo_motivo:        req.body.id_tipo_motivo,
        id_horario:            req.body.id_horario,
        id_empleado:           req.body.id_empleado,
        id_promocion:          req.body.id_promocion,
        preferencia_masajista: req.body.preferencia_masajista,  
        fecha:                 req.body.fecha,     
        estatus:               req.body.estatus
      }
      
      solicitud.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'solicitud actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Solicitud.forge(conditions).fetch()
    .then(function(solicitud){
      if(!solicitud) return res.status(404).json({ error : true, data : { message : 'solicitud no existe' } });

      Solicitud.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'solicitud eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}