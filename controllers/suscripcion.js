//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");

//----dependencias------  
const Usuario = require('../models/usuario');
const Cliente = require('../models/cliente');
const Negocio = require('../models/negocio');
const jwt = require('../services/jwt');
const mailer = require('../services/mailer');

//----signup------  
function signUp(req,res) {

	console.log("Estoy en signUp");
	console.log("Esto es req.body:");
	console.log(req.body);

	//---- encriptado de contraseña
	let salt = bcrypt.genSaltSync(12);
	let hash = bcrypt.hashSync(req.body.contrasenia, salt);
	
	let newUser = {
	    correo:         req.body.correo,
	    contrasenia:    hash,
	  }

  	Usuario.forge(newUser).save()
	.then(function(usuario){

		let newClient = {
		    nombres:             req.body.nombre,
		    apellidos:           req.body.apellido,
		    cedula:             req.body.cedula,
		    telefono:           req.body.telefono,
		    direccion:          req.body.direccion,
		    id_ciudad:          req.body.ciudad,
			fecha_nacimiento:   req.body.fecha_nacimiento,
			imagen:				'url', // Cambiar
		    id_usuario:         usuario.id,
		}

		console.log("newClient", newClient);

		Cliente.forge(newClient).save()
		.then(function(cliente){

			if(req.body.perfil != undefined) {
				var perfil = JSON.parse(req.body.perfil);
				var valoresParametro = [];
				for (var i = 0; i < perfil.length; i++) {
					valoresParametro.push(perfil[i].id);
				}
				if(valoresParametro.length > 0) {
					cliente.valor_parametro().attach(valoresParametro);
				}
			}
			
			Negocio.forge().fetch()
			.then(function(negocio) {
				//--- Enviar Correo ---
				mailer.enviarCorreo(newUser, req.body.contrasenia, newClient, negocio);
				//--- Respuesta exitosa ---
				res.status(200).json({ error: false, data: { message : 'Registro exitoso' } });
			})

		})
		.catch(function (err2) {
			res.status(500).json({ error: true, data: { message: err2.message } });
		});

	})
	.catch(function (err3) {
		res.status(500).json({ error: true, data: { message: err3.message } });
	});
}
//----signin------  
function signIn(req,res) {
	
  let conditions = { correo: req.body.correo };

  Usuario.forge(conditions).fetch()
	.then(function(usuario){
		if(!usuario) return res.status(404).send({message:"El usuario no existe"})
        
        let isPassword = bcrypt.compareSync(req.body.contrasenia, usuario.get("contrasenia"))
		if(isPassword){
			res.status(200).send({ error: false, data: { message:"Te has logueado de forma exitosa", token: jwt.createToken(usuario) } })
		}else{
			res.status(404).send({ error: true, data: {message: "La contraseña es incorrecta"} })
		}
	})
	.catch(function(err){
		res.status(500).send({ error: true, data: { message: err.message }})
	})

}

//----exports------  
module.exports = {
	signUp,
	signIn
}