//----dependencias------  
'use strict'
const Vista_parametros_preferencias_clientes = require('../models/vista_parametros_preferencias_clientes');

exports.findDocuments = (req,res) => {
    
  Vista_parametros_preferencias_clientes.forge().fetchAll()
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}