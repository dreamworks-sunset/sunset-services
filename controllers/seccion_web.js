//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Seccion_web = require('../models/seccion_web');

exports.findDocuments = (req,res) => {
  
    Seccion_web.forge().fetchAll({withRelated:['imagen']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {

    nombre:       req.body.nombre,
    menu_web:     req.body.menu_web,
     
  }

  Seccion_web.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'seccion_web creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Seccion_web.forge(conditions).fetch({withRelated:['imagen']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'seccion_web no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Seccion_web.forge(conditions).fetch()
    .then(function(seccion_web){
      if(!seccion_web) return res.status(404).json({ error : true, data : { message : 'seccion_web no existe' } });

      let updateData = {
        id:                    req.params.id,
        nombre:                 req.body.nombre,
        menu_web:               req.body.menu_web,
        fecha_creacion:         req.body.fecha_creacion,       
        estatus:               req.body.estatus,
        
      }
      
      seccion_web.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'seccion_web actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Seccion_web.forge(conditions).fetch()
    .then(function(seccion_web){
      if(!seccion_web) return res.status(404).json({ error : true, data : { message : 'seccion_web no existe' } });

      Seccion_web.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'seccion_web eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}