//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Vista_citas_no_atendidas = require('../models/vista_citas_no_atendidas');

exports.findDocuments = (req,res) => {
    
  Vista_citas_no_atendidas.forge().fetchAll()
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Vista_citas_no_atendidas.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'valor_parametro no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}
