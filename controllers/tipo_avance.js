//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const tipo_avance = require('../models/tipo_avance');

exports.findDocuments = (req,res) => {
  
  tipo_avance.forge().fetchAll(/*{withRelated:['avance']}*/)
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    descripcion:         req.body.descripcion,
  }

  tipo_avance.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'tipo_avance creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_avance.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'tipo_avance no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_avance.forge(conditions).fetch()
    .then(function(tipo_avance){
      if(!tipo_avance) return res.status(404).json({ error : true, data : { message : 'tipo_avance no existe' } });

      let updateData = {
        descripcion:         req.body.descripcion,
      }
      
      tipo_avance.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'tipo_avance actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_avance.forge(conditions).fetch()
    .then(function(tipo_avance){
      if(!tipo_avance) return res.status(404).json({ error : true, data : { message : 'tipo_avance no existe' } });

      tipo_avance.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'tipo_avance eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}