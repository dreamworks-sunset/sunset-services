//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Tipo_servicio = require('../models/tipo_servicio');
const cloudinary = require('../cloudinary'); // importar archivo para guardar imagen

exports.findDocuments = (req,res) => {
  
  Tipo_servicio.forge().fetchAll({withRelated:['servicio','empleado.horario_empleado.horario.bloque_hora','empleado.horario_empleado.horario.dia_laborable' , 'empleado.horario_empleado.agenda.cita.orden_servicio.solicitud.servicio' ]})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });
}

exports.createDocument = (req,res) => {
  const imagen = req.files.imagen//Se crea una constante que recibe el objeto de la imagen
  console.log(req.body);
  cloudinary.uploader.upload(imagen.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion

    if (result.error) {
      return res.status(500).json({
          error: true,
          data: { message: result.error }
        });
    } 
    console.log(req.body);
  let newData = {  
    descripcion:         req.body.descripcion,
    imagen:              result.url,//La funcion anterior devuelve un resultado, y ese objeto se llama su atributo url y se obtiene la url donde fue guardada la imagen en cloudinary
  }

  Tipo_servicio.forge(newData).save()
    .then(function(data){

      res.status(200).json({ error: false, data: { message: 'tipo_servicio creado' } });
    })
    .catch(function (err) {
      res.status(500).json({ error: true, data: {message: err.message} });
    });
  })
}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Tipo_servicio.forge(conditions).fetch({withRelated:['servicio','empleado.horario_empleado.horario.bloque_hora','empleado.horario_empleado.horario.dia_laborable' , 'empleado.horario_empleado.agenda.cita.orden_servicio.solicitud.servicio' ]})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'tipo_servicio no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {
  console.log(req.body);
  if(req.body.cambio_estatus != 'true'){
  
              
            
  const imagen = req.files.imagen//Se crea una constante que recibe el objeto de la imagen
  console.log('imagen =', imagen);
  var resultado;
  if (imagen.originalFilename != ''){
  cloudinary.uploader.upload(imagen.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion
  
        if (result.error) {
          return res.status(500).json({
              error: true,
              data: { message: result.error }
            });
        } 
        resultado = result;
      });
    }
    let conditions = { id: req.params.id };
  
    Tipo_servicio.forge(conditions).fetch()
      .then(function(tipo_servicio){
        if(!tipo_servicio) return res.status(404).json({ error : true, data : { message : 'tipo_servicio no existe' } });
        var imagenCondicion = (resultado==undefined) ? tipo_servicio.imagen : resultado.url;
        let updateData = {
          id:                   req.params.id,
          descripcion:          req.body.descripcion,
          imagen:               imagenCondicion,//La funcion anterior devuelve un resultado, y ese objeto se llama su atributo url y se obtiene la url donde fue guardada la imagen en cloudinary      
          estatus:              req.body.estatus
        }
        console.log(req.body)
        tipo_servicio.save(updateData)
          .then(function(data){
  
            res.status(200).json({ error : false, data : { message : 'tipo_servicio actualizado'} });
          })
          .catch(function(err){
            res.status(500).json({ error : false, data : {message : err.message} });
          })
  
      })
      .catch(function(err){
            res.status(500).json({ error : false, data : {message : err.message} })
      })
      
      //Fin imagen
      } else
      {
  let conditions = { id: req.params.id };
  
    Tipo_servicio.forge(conditions).fetch()
      .then(function(tipo_servicio){
        if(!tipo_servicio) return res.status(404).json({ error : true, data : { message : 'tipo_servicio no existe' } });
  
        let updateData = {
            estatus:              req.body.estatus
        }
        
        tipo_servicio.save(updateData)
          .then(function(data){
  
            res.status(200).json({ error : false, data : { message : 'tipo_servicio actualizado'} });
          })
          .catch(function(err){
            res.status(500).json({ error : false, data : {message : err.message} });
          })
  
      })
      .catch(function(err){
            res.status(500).json({ error : false, data : {message : err.message} })
      })
      }
  
  }

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Tipo_servicio.forge(conditions).fetch()
    .then(function(tipo_servicio){
      if(!tipo_servicio) return res.status(404).json({ error : true, data : { message : 'tipo_servicio no existe' } });

      tipo_servicio.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'tipo_servicio eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}