//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Marca = require('../models/marca');

exports.findDocuments = (req,res) => {
  
  Marca.forge().fetchAll({withRelated:['insumo','proveedor']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

 
  console.log(req.body);
  let newData = {
    nombre:                     req.body.nombre,
    precio_por_sesion:         req.body.precio_por_sesion,
   

 
     }  

  Marca.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'marca creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Marca.forge(conditions).fetch({withRelated:['insumo','proveedor']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'marca no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Marca.forge(conditions).fetch()
    .then(function(marca){
      if(!marca) return res.status(404).json({ error : true, data : { message : 'marca no existe' } });

      let updateData = {
      nombre:                     req.body.nombre,
    precio_por_sesion:            req.body.precio_por_sesion,
    estatus:                      req.body.estatus,
      }
      
      marca.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'marca actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Marca.forge(conditions).fetch()
    .then(function(marca){
      if(!marca) return res.status(404).json({ error : true, data : { message : 'marca no existe' } });

      Marca.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'marca eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}