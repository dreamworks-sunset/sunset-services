//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Red_social_negocio = require('../models/red_social_negocio');

exports.findDocuments = (req,res) => {
  
  Red_social_negocio.forge().fetchAll({withRelated:['negocio','red_social']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {
    id_red_social:  req.body.id_red_social,
    id_negocio:     '1',
    url:            req.body.url
  }

  Red_social_negocio.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'red_social_negocio creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Red_social_negocio.forge(conditions).fetch({withRelated:['negocio','red_social']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'red_social_negocio no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Red_social_negocio.forge(conditions).fetch()
    .then(function(red_social_negocio){
      if(!red_social_negocio) return res.status(404).json({ error : true, data : { message : 'red_social_negocio no existe' } });

      let updateData = {
        id_red_social:  req.body.id_red_social,
        id_negocio:     '1',
        url:            req.body.url,
        estatus:        req.body.estatus
      }
      
      red_social_negocio.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'red_social_negocio actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Red_social_negocio.forge(conditions).fetch()
    .then(function(red_social_negocio){
      if(!red_social_negocio) return res.status(404).json({ error : true, data : { message : 'red_social_negocio no existe' } });

      Red_social_negocio.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'red_social_negocio eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}