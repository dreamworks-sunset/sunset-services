//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Rol = require('../models/rol');

exports.findDocuments = (req,res) => {
  
  Rol.forge().where('id', '<>', 0).fetchAll({
    withRelated: [
      'tipo_rol',
      'usuario',
      'menu_opcion',
      'menu_opcion.menu_opcion',
      'menu_opcion.menu_opcion.menu_opcion',
      'menu_opcion.menu_opcion_padre',
      'menu_opcion.menu_opcion_padre.menu_opcion',
      'menu_opcion.menu_opcion_padre.menu_opcion_padre']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {
    nombre:         req.body.nombre,
    descripcion:    req.body.descripcion,
    id_tipo_rol:    req.body.id_tipo_rol
  }

  Rol.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'rol creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Rol.forge(conditions).where('id', '<>', 0).fetch({
    withRelated: [
      'tipo_rol',
      'usuario.rol.menu_opcion.menu_opcion_padre.menu_opcion_padre',
      'menu_opcion.menu_opcion_padre.menu_opcion_padre'
    ]
  })
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'rol no existe' } });
      
      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

   if(req.body.cambio_estatus != 'true') { 

  let conditions = { id: req.params.id };

  Rol.forge(conditions).fetch({
    withRelated: [
      'tipo_rol',
      'usuario.rol.menu_opcion.menu_opcion_padre.menu_opcion_padre',
      'menu_opcion.menu_opcion_padre.menu_opcion_padre'
    ]
  })
    .then(function(rol){
      if(!rol) return res.status(404).json({ error : true, data : { message : 'rol no existe' } });

      let updateData = {
        id:             req.params.id,
        nombre:         req.body.nombre,
        descripcion:    req.body.descripcion,
        id_tipo_rol:    req.body.id_tipo_rol,
        estatus:        req.body.estatus
      }
      
      rol.save(updateData)
        .then(function(data){
          if(req.body.menu_opcion_eliminar != undefined && req.body.menu_opcion != undefined) {
            var permisosPorEliminar = JSON.parse('[' + req.body.menu_opcion_eliminar + ']');
            var permisos = JSON.parse('[' + req.body.menu_opcion + ']');

            //Eliminamos los permisos que ya no se asocian con el rol
            if(permisosPorEliminar.length != 0) {       
              data.menu_opcion().detach(permisosPorEliminar);
            }

            //Añadimos los nuevos permisos que se asocian con el rol
            if(permisos.length != 0) {        
              data.menu_opcion().attach(permisos);
            }
          }
          res.status(200).json({ error : false, data : data.toJSON(), message : 'rol actualizado' });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })


     } else {
    let conditions = { id: req.params.id };
    console.log(req.body);
    Rol.forge(conditions).fetch()
      .then(function(rol){
        if(!rol) return res.status(404).json({ error : true, data : { message : 'rol no existe' } });

        let updateData = {
          estatus:                         req.body.estatus,
        }
      
      rol.save(updateData)
        .then(function(data){
          
          res.status(200).json({ error : false, data : { message : 'rol actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });

        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })
  }

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Rol.forge(conditions).fetch()
    .then(function(rol){
      if(!rol) return res.status(404).json({ error : true, data : { message : 'rol no existe' } });

      Rol.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'rol eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}