//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const tipo_comentario = require('../models/tipo_comentario');

exports.findDocuments = (req,res) => {
  
  tipo_comentario.forge().fetchAll({withRelated:['comentario']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    descripcion:         req.body.descripcion,
  }

  tipo_comentario.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'tipo_comentario creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_comentario.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'tipo_comentario no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_comentario.forge(conditions).fetch()
    .then(function(tipo_comentario){
      if(!tipo_comentario) return res.status(404).json({ error : true, data : { message : 'tipo_comentario no existe' } });

      let updateData = {
        descripcion:         req.body.descripcion,
      }
      
      tipo_comentario.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'tipo_comentario actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_comentario.forge(conditions).fetch()
    .then(function(tipo_comentario){
      if(!tipo_comentario) return res.status(404).json({ error : true, data : { message : 'tipo_comentario no existe' } });

      tipo_comentario.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'tipo_comentario eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}