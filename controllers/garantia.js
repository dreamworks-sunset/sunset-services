//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Garantia = require('../models/garantia');

exports.findDocuments = (req,res) => {
  
  Garantia.forge().fetchAll({withRelated:['orden_servicio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  var fechaIStr = req.body.fecha_inicio.split('/');
  var fechaFStr = req.body.fecha_fin.split('/');

  var anioI = fechaIStr[2];
  var mesI = fechaIStr[1];
  var diaI = fechaIStr[0];

  var anioF = fechaFStr[2];
  var mesF = fechaFStr[1];
  var diaF = fechaFStr[0];

  console.log(req.body);
  let newData = {
    id_orden_servicio:          req.body.id_orden_servicio,
    fecha_inicio:               anioI + '-' + mesI + '-' + diaI,
    fecha_fin:                  anioF + '-' + mesF + '-' + diaF,
     }  

  Garantia.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'garantia creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Garantia.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'garantia no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Garantia.forge(conditions).fetch()
    .then(function(garantia){
      if(!garantia) return res.status(404).json({ error : true, data : { message : 'garantia no existe' } });

      let updateData = {
    id_orden_servicio:          req.body.id_orden_servicio,
    fecha_inicio:               anioI + '-' + mesI + '-' + diaI,
    fecha_fin:                  anioF + '-' + mesF + '-' + diaF,
     estatus:                   req.body.estatus,
      }
      
      garantia.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'garantia actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Garantia.forge(conditions).fetch()
    .then(function(garantia){
      if(!garantia) return res.status(404).json({ error : true, data : { message : 'garantia no existe' } });

      Garantia.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'garantia eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}