//----dependencias------  
'use strict'
const Empleado_tipo_servicio = require('../models/empleado_tipo_servicio');

exports.findDocuments = (req,res) => {
  
  Empleado_tipo_servicio.forge().fetchAll()
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}