//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Condicion_garantia = require('../models/condicion_garantia');

exports.findDocuments = (req,res) => {
  
  Condicion_garantia.forge().fetchAll({withRelated:['servicio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {
    clausula:         req.body.clausula
  }

  Condicion_garantia.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'condicion_garantia creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Condicion_garantia.forge(conditions).fetch({withRelated:['servicio']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'condicion_garantia no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Condicion_garantia.forge(conditions).fetch()
    .then(function(condicion_garantia){
      if(!condicion_garantia) return res.status(404).json({ error : true, data : { message : 'condicion_garantia no existe' } });

      let updateData = {
        id:             req.params.id,
        clausula:         req.body.clausula,
        estatus:        req.body.estatus
      }
      
      condicion_garantia.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'condicion_garantia actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Condicion_garantia.forge(conditions).fetch()
    .then(function(condicion_garantia){
      if(!condicion_garantia) return res.status(404).json({ error : true, data : { message : 'condicion_garantia no existe' } });

      Condicion_garantia.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'condicion_garantia eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}