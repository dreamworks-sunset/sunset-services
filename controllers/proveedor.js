//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Proveedor = require('../models/proveedor');

exports.findDocuments = (req,res) => {
  
  Proveedor.forge().fetchAll({withRelated:['ciudad' , 'insumo', 'marca']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

 
  console.log(req.body);
  let newData = {
    nombre:                       req.body.nombre,
    rif:                          req.body.rif,
    direccion:                    req.body.direccion,
    telefono_1:                   req.body.telefono_1,
    telefono_2:                   req.body.telefono_2,
    telefono_3:                   req.body.telefono_3,
    correo:                       req.body.correo,
    id_ciudad:                     req.body.id_ciudad,

 
     }  

  Proveedor.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'proveedor creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Proveedor.forge(conditions).fetch({withRelated:['ciudad' , 'insumo', 'marca']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'proveedor no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Proveedor.forge(conditions).fetch()
    .then(function(proveedor){
      if(!proveedor) return res.status(404).json({ error : true, data : { message : 'proveedor no existe' } });

      let updateData = {
    nombre:                       req.body.nombre,
    rif:                          req.body.rif,
    direccion:                    req.body.direccion,
    telefono_1:                   req.body.telefono_1,
    telefono_2:                   req.body.telefono_2,
    telefono_3:                   req.body.telefono_3,
    correo:                       req.body.correo,
    id_ciudad:                     req.body.id_ciudad,

    estatus:                      req.body.estatus,
      }
      
      proveedor.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'proveedor actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Proveedor.forge(conditions).fetch()
    .then(function(proveedor){
      if(!proveedor) return res.status(404).json({ error : true, data : { message : 'proveedor no existe' } });

      Proveedor.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'proveedor eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}