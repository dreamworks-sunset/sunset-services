//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Promocion = require('../models/promocion');
const cloudinary = require('../cloudinary'); //Importar para poder usar

exports.findDocuments = (req,res) => {
  
  Promocion.forge().fetchAll({withRelated:['servicio','detalle_promocion']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  const imagen = req.files.imagen//Se crea una constante que recibe el objeto de la imagen

  cloudinary.uploader.upload(imagen.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion
    if (result.error) {
      return res.status(500).json({
          error: true,
          data: { message: result.error }
        });
      } 

  var fechaIStr = req.body.fecha_inicio.split('/');
  var fechaFStr = req.body.fecha_fin.split('/');

  var anioI = fechaIStr[2];
  var mesI = fechaIStr[1];
  var diaI = fechaIStr[0];

  var anioF = fechaFStr[2];
  var mesF = fechaFStr[1];
  var diaF = fechaFStr[0];

  console.log(req.body);
  let newData = {
    nombre:                     req.body.nombre,
    descripcion:                req.body.descripcion,
    id_servicio:                req.body.id_servicio,
    porcentaje_descuento:       req.body.porcentaje_descuento,
    fecha_inicio:               anioI + '-' + mesI + '-' + diaI,
    fecha_fin:                  anioF + '-' + mesF + '-' + diaF,
    imagen:                     result.url,//La funcion anterior devuelve un resultado, y ese objeto se llama su atributo url y se obtiene la url donde fue guardada la imagen en cloudinary
    
     }  

    Promocion.forge(newData).save()
    .then(function(data){
    var valoresParametroPromocion = JSON.parse('[' + req.body.detalle_promocion + ']');
      
       if(valoresParametroPromocion.length != 0) {       
        data.detalle_promocion().attach(valoresParametroPromocion);
      }

      res.status(200).json({ error: false, data: { message: 'promocion creada' } });
    })
    .catch(function (err) {
      res.status(500).json({ error: true, data: {message: err.message} });
    });
  })

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Promocion.forge(conditions).fetch({withRelated:['servicio','detalle_promocion']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'promocion no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  if(req.body.cambio_estatus != 'true') {

  const imagen = req.files.imagen//Se crea una constante que recibe el objeto de la imagen
    var resultado;
    if (imagen.originalFilename != ''){
  cloudinary.uploader.upload(imagen.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion
    if (result.error) {
      return res.status(500).json({
          error: true,
          data: { message: result.error }
        });
      }
      resultado = result;
  });
}
    // Fin cloudinary
  let conditions = { id: req.params.id };
  console.log(req.body);
  Promocion.forge(conditions).fetch()
    .then(function(promocion){
      if(!promocion) return res.status(404).json({ error : true, data : { message : 'promocion no existe' } });

      var imagen = (resultado == undefined) ? promocion.imagen : resultado.url;

      var fechaIStr = req.body.fecha_inicio.split('/');
      var fechaFStr = req.body.fecha_fin.split('/');

      var anioI = fechaIStr[2];
      var mesI = fechaIStr[1];
      var diaI = fechaIStr[0];

      var anioF = fechaFStr[2];
      var mesF = fechaFStr[1];
      var diaF = fechaFStr[0];

    let updateData = {
    nombre:                     req.body.nombre,
    descripcion:                req.body.descripcion,
    id_servicio:                req.body.id_servicio,
    porcentaje_descuento:       req.body.porcentaje_descuento,
    imagen:                     imagen,//La funcion anterior devuelve un resultado, y ese objeto se llama su atributo url y se obtiene la url donde fue guardada la imagen en cloudinary
    fecha_inicio:               anioI + '-' + mesI + '-' + diaI,
    fecha_fin:                  anioF + '-' + mesF + '-' + diaF,
     estatus:                   req.body.estatus
      }
      
      promocion.save(updateData)
        .then(function(data){

          if(req.body.cambio_estatus != 'true')
          {
              var valoresParametroPromocionPorEliminar = JSON.parse('[' + req.body.detalle_promocion_eliminar + ']');
              var valoresParametroPromocion = JSON.parse('[' + req.body.detalle_promocion + ']');

                 //Eliminamos los valores parámetros que ya no se asocian con el servicio
              if(valoresParametroPromocionPorEliminar.length != 0) {       
                data.detalle_promocion().detach(valoresParametroPromocionPorEliminar);
              }

              //Añadimos los nuevos valores parámetros que se asocian con el servicio
              if(valoresParametroPromocion.length != 0) {        
                data.detalle_promocion().attach(valoresParametroPromocion);
              }
          }
          res.status(200).json({ error : false, data : { message : 'promocion actualizada'} });
         
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })
    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })
  
} else {
  let conditions = { id: req.params.id };
  console.log(req.body);
  Promocion.forge(conditions).fetch()
    .then(function(promocion){
      if(!promocion) return res.status(404).json({ error : true, data : { message : 'promocion no existe' } });

    let updateData = {
     estatus:                   req.body.estatus
      }
      
      promocion.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'promocion actualizada'} });
         
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })
    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })
}
}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Promocion.forge(conditions).fetch()
    .then(function(promocion){
      if(!promocion) return res.status(404).json({ error : true, data : { message : 'promocion no existe' } });

      Promocion.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'promocion eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}