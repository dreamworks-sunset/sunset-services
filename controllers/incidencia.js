//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Incidencia = require('../models/incidencia');

exports.findDocuments = (req,res) => {
  
  Incidencia.forge().fetchAll({withRelated:['tipo_incidencia' , 'clasificacion_incidencia' , 'sesion.cita.orden_servicio.solicitud.cliente','sesion.cita.orden_servicio.solicitud.empleado','sesion.cita.orden_servicio.solicitud.servicio' , 'insumo']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

 
  console.log(req.body);
  let newData = {
    id_sesion:                     req.body.id_sesion,
    id_clasificacion_incidencia:   req.body.id_clasificacion_incidencia,
    id_tipo_incidencia:              req.body.id_tipo_incidencia,
    contenido:                     req.body.contenido,

 
     }  

  Incidencia.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'incidencia creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Incidencia.forge(conditions).fetch({withRelated:['tipo_incidencia' , 'clasificacion_incidencia' , 'sesion.cita.orden_servicio.solicitud.cliente' ,'sesion.cita.orden_servicio.solicitud.empleado' ,'sesion.cita.orden_servicio.solicitud.servicio', 'insumo']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'incidencia no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Incidencia.forge(conditions).fetch()
    .then(function(incidencia){
      if(!incidencia) return res.status(404).json({ error : true, data : { message : 'incidencia no existe' } });

      let updateData = {
    id_sesion:                     req.body.id_sesion,
    id_clasificacion_incidencia:   req.body.id_clasificacion_incidencia,
    id_tipo_incidencia:            req.body.id_tipo_incidencia,
    contenido:                     req.body.contenido,
    estatus:                      req.body.estatus,
      }
      
      incidencia.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'incidencia actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Incidencia.forge(conditions).fetch()
    .then(function(incidencia){
      if(!incidencia) return res.status(404).json({ error : true, data : { message : 'incidencia no existe' } });

      Incidencia.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'incidencia eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}