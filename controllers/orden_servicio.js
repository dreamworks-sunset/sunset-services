//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Orden_servicio = require('../models/orden_servicio');
const Cita = require('../models/cita');

exports.findDocuments = (req,res) => {
  
  Orden_servicio.forge().fetchAll({withRelated:['solicitud.bloque_hora' , 'orden_servicio_garantia' , 'orden_servicio' , 'cita' , 'reclamo' , 'garantia','solicitud.cliente','solicitud.promocion','solicitud.servicio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

 
  console.log(req.body);
  let newData = {
    id_solicitud:                     req.body.id_solicitud,
    id_orden_servicio_garantia:   req.body.id_orden_servicio_garantia,
  

 
     }  

  Orden_servicio.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'orden_servicio creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Orden_servicio.forge(conditions).fetch({withRelated:['solicitud.bloque_hora' , 'orden_servicio_garantia' , 'orden_servicio' , 'cita' , 'reclamo' , 'garantia','solicitud.cliente','solicitud.promocion','solicitud.servicio']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'orden_servicio no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Orden_servicio.forge(conditions).fetch({withRelated:['solicitud.bloque_hora' , 'orden_servicio_garantia' , 'orden_servicio' , 'cita' , 'reclamo' , 'garantia','solicitud.cliente','solicitud.promocion','solicitud.servicio']})
    .then(function(orden_servicio){
      if(!orden_servicio) return res.status(404).json({ error : true, data : { message : 'orden_servicio no existe' } });
      console.log(req.body.estatus);
      let updateData = {
          estatus:     req.body.estatus,
      }
      
      orden_servicio.save(updateData)
        .then(function(data){
          let newData = {
              id_orden_servicio:      req.params.id, 
              id_empleado:            req.body.id_empleado,
              fecha:                  req.body.fecha,
               }  

            Cita.forge(newData).save()
            .then(function(data2){
              res.status(200).json({ error: false, data: { message: 'cita creada' } });
            })
            .catch(function (err) {
              res.status(500).json({ error: true, data: {message: err.message} });
            });
          
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Orden_servicio.forge(conditions).fetch()
    .then(function(orden_servicio){
      if(!orden_servicio) return res.status(404).json({ error : true, data : { message : 'orden_servicio no existe' } });

     Orden_servicio.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'orden_servicio eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}