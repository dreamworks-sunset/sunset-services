//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const respuesta_comentario = require('../models/respuesta_comentario');
const Comentario = require('../models/comentario');

exports.findDocuments = (req,res) => {
  
  respuesta_comentario.forge().fetchAll({withRelated:['comentario','tipo_respuesta_comentario']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    id_comentario:                req.body.id_comentario,
    id_tipo_respuesta_comentario: req.body.id_tipo_respuesta_comentario,
    contenido:                    req.body.contenido
  }

  respuesta_comentario.forge(newData).save()
  .then(function(data){
    var id_comentario = data.get('id_comentario');
    Comentario.forge({id: id_comentario}).fetch()
    .then(function(comentario) {
      let updateData = {
        estatus: 2
      }
      comentario.save(updateData)
      .then(function(data){
          console.log('Comentario respondido');
        })
      .catch(function(err){
        res.status(500).json({ error : true, data : {message : err.message} })
      });
    })
    .catch();
    res.status(200).json({ error: false, data: { message: 'respuesta_comentario creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  respuesta_comentario.forge(conditions).fetch({withRelated:['comentario','tipo_respuesta_comentario']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'respuesta_comentario no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  respuesta_comentario.forge(conditions).fetch()
    .then(function(respuesta_comentario){
      if(!respuesta_comentario) return res.status(404).json({ error : true, data : { message : 'tipo_respuesta_comentario no existe' } });

      let updateData = {
        id_comentario:                req.body.id_comentario,
        id_tipo_respuesta_comentario: req.body.id_tipo_respuesta_comentario,
        contenido:                    req.body.contenido,
        estatus:                      req.body.estatus
      }
      
      respuesta_comentario.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'respuesta_comentario actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  respuesta_comentario.forge(conditions).fetch()
    .then(function(respuesta_comentario){
      if(!respuesta_comentario) return res.status(404).json({ error : true, data : { message : 'respuesta_comentario no existe' } });

      respuesta_comentario.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'respuesta_comentario eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}