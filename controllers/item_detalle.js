//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Item_detalle = require('../models/item_detalle');

exports.findDocuments = (req,res) => {
  
  Item_detalle.forge().fetchAll(/*{withRelated:['detalle_sesion']}*/)
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    descripcion:         req.body.descripcion,
  }

  Item_detalle.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'item_detalle creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Item_detalle.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'item_detalle no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Item_detalle.forge(conditions).fetch()
    .then(function(item_detalle){
      if(!item_detalle) return res.status(404).json({ error : true, data : { message : 'item_detalle no existe' } });

      let updateData = {
        descripcion:         req.body.descripcion,
      }
      
      item_detalle.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'item_detalle actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Item_detalle.forge(conditions).fetch()
    .then(function(item_detalle){
      if(!item_detalle) return res.status(404).json({ error : true, data : { message : 'item_detalle no existe' } });

      item_detalle.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'item_detalle eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}