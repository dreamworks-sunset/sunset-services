//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Cliente = require('../models/cliente');
const cloudinary = require('../cloudinary');

exports.findDocuments = (req,res) => {
  
  Cliente.forge().fetchAll({withRelated:['usuario' , 'perfil.valor_parametro.parametro.tipo_parametro' , 'solicitud']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  var fechaIStr = req.body.fecha_inicio.split('/');
  var fechaFStr = req.body.fecha_fin.split('/');

  var anioI = fechaIStr[2];
  var mesI = fechaIStr[1];
  var diaI = fechaIStr[0];

  var anioF = fechaFStr[2];
  var mesF = fechaFStr[1];
  var diaF = fechaFStr[0];

  console.log(req.body);
  let newData = {
    nombres:                     req.body.nombres,
    apellidos:                   req.body.apellidos,
    cedula:                      req.body.cedula,
    telefono:                    req.body.telefono,
    imagen:                      'url',
    fecha_nacimiento:            anioI + '-' + mesI + '-' + diaI,
    id_ciudad:                   req.body.id_ciudad,
    
    
     }  

  Cliente.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'cliente creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Cliente.forge(conditions).fetch({withRelated:['usuario' ,'perfil.valor_parametro.parametro.tipo_parametro','perfil.valor_parametro.unidad','solicitud']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'cliente no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Cliente.forge(conditions).fetch()
    .then(function(cliente){
      if(!cliente) return res.status(404).json({ error : true, data : { message : 'cliente no existe' } });

       


      let updateData = {
    nombres:                     req.body.nombres,
    apellidos:                   req.body.apellidos,
    cedula:                      req.body.cedula,
    telefono:                    req.body.telefono,
   // imagen:                      'url',
   // fecha_nacimiento:            anioI + '-' + mesI + '-' + diaI,
    es_real:                     req.body.es_real,
    id_ciudad:                   req.body.id_ciudad,
    id_cliente:                  req.body.id_cliente,
     estatus:                    req.body.estatus
      }
      
      cliente.save(updateData)
        .then(function(data){


          var valoresPerfil = JSON.parse(req.body.perfil);

          if(valoresPerfil.length != 0) {       
            data.valor_parametro().attach(valoresPerfil);
          }

          res.status(200).json({ error : false, data : { message : 'cliente actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.updateDocumentMovil = (req,res) => {

  /*const imagen = req.files.imagen
  cloudinary.uploader.upload(imagen.path, function(result) {
    if (result.error) {
      console.log('Primero');
      return res.status(500).json({
          error: true,
          data: { message: result.error }
        });
    } 
*/
  let conditions = { id: req.params.id };
console.log(req.body);
  Cliente.forge(conditions).fetch()
    .then(function(Cliente){
      if(!Cliente) return res.status(404).json({ error : true, data : { message : 'cliente no existe' } });

      let updateData = {
    nombres:                     req.body.nombres,
    apellidos:                   req.body.apellidos,
    telefono:                    req.body.telefono,
    direccion:                   req.body.direccion,
      }
      
      Cliente.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'cliente actualizado'} });
        })
        .catch(function(err){
          console.log('Segundo');
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
      console.log('Tercero');
          res.status(500).json({ error : true, data : {message : err.message} })
    })

  //})
}

exports.updateDocumentMovilImagen = (req,res) => {
 const imagen = req.files.imagen
  cloudinary.uploader.upload(imagen.path, function(result) {
    if (result.error) {
      console.log('Primero movil');
      return res.status(500).json({
          error: true,
          data: { message: result.error }
        });
    } 

let conditions = { id: req.params.id };
console.log(req.body);
  Cliente.forge(conditions).fetch()
    .then(function(Cliente){
      if(!Cliente) return res.status(404).json({ error : true, data : { message : 'cliente no existe' } });

      let updateData = {
    imagen:                     result.url
      }
      
      Cliente.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'cliente actualizado'} });
        })
        .catch(function(err){
          console.log('Segundo');
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
      console.log('Tercero');
          res.status(500).json({ error : true, data : {message : err.message} })
    })

  })
}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Cliente.forge(conditions).fetch()
    .then(function(cliente){
      if(!cliente) return res.status(404).json({ error : true, data : { message : 'cliente no existe' } });

      Cliente.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'cliente eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}