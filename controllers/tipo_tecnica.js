//---dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const tipo_tecnica = require('../models/tipo_tecnica');

exports.findDocuments = (req,res) => {
  
  tipo_tecnica.forge().fetchAll({withRelated:['tecnica']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    descripcion:         req.body.descripcion,
  }

  tipo_tecnica.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'tipo_tecnica creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_tecnica.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'tipo_tecnica no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_tecnica.forge(conditions).fetch()
    .then(function(tipo_tecnica){
      if(!tipo_tecnica) return res.status(404).json({ error : true, data : { message : 'tipo_tecnica no existe' } });

      let updateData = {
        descripcion:         req.body.descripcion,
      }
      
      tipo_tecnica.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'tipo_tecnica actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_tecnica.forge(conditions).fetch()
    .then(function(tipo_tecnica){
      if(!tipo_tecnica) return res.status(404).json({ error : true, data : { message : 'tipo_tecnica no existe' } });

      tipo_tecnica.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'tipo_tecnica eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}