//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const tipo_motivo = require('../models/tipo_motivo');

exports.findDocuments = (req,res) => {
  
  tipo_motivo.forge().fetchAll(/*{withRelated:['solicitud']}*/)
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    descripcion:         req.body.descripcion,
  }

  tipo_motivo.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'tipo_motivo creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_motivo.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'tipo_motivo no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_motivo.forge(conditions).fetch()
    .then(function(tipo_motivo){
      if(!tipo_motivo) return res.status(404).json({ error : true, data : { message : 'tipo_motivo no existe' } });

      let updateData = {
        descripcion:         req.body.descripcion,
      }
      
      tipo_motivo.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'tipo_motivo actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_motivo.forge(conditions).fetch()
    .then(function(tipo_motivo){
      if(!tipo_motivo) return res.status(404).json({ error : true, data : { message : 'tipo_motivo no existe' } });

      tipo_motivo.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'tipo_motivo eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}