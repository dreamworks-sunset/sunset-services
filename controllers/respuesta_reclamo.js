//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Respuesta_reclamo = require('../models/respuesta_reclamo');
const Reclamo = require('../models/reclamo');

exports.findDocuments = (req,res) => {
  
  Respuesta_reclamo.forge().fetchAll({withRelated:['tipo_respuesta_reclamo' , 'reclamo.orden_servicio.solicitud.servicio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

 
  console.log(req.body);
  let newData = {
    id_reclamo:                           req.body.id_reclamo,
    id_tipo_respuesta_reclamo:            req.body.id_tipo_respuesta_reclamo,

 
     }  

  Respuesta_reclamo.forge(newData).save()
  .then(function(data){

       var id_reclamo = data.get('id_reclamo');
    Reclamo.forge({id: id_reclamo}).fetch()
    .then(function(reclamo) {
      let updateData = {
        estatus: 2
      }
      reclamo.save(updateData)
      .then(function(data){
          console.log('Reclamo respondido');
        })
      .catch(function(err){
        res.status(500).json({ error : true, data : {message : err.message} })
      });
    })


    res.status(200).json({ error: false, data: { message: 'respuesta_reclamo creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Respuesta_reclamo.forge(conditions).fetch({withRelated:['tipo_respuesta_reclamo' , 'reclamo.orden_servicio.solicitud.servicio']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'respuesta_reclamo no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Respuesta_reclamo.forge(conditions).fetch()
    .then(function(respuesta_reclamo){
      if(!respuesta_reclamo) return res.status(404).json({ error : true, data : { message : 'respuesta_reclamo no existe' } });

      let updateData = {
    id_reclamo:                           req.body.id_reclamo,
    id_tipo_respuesta_reclamo:            req.body.id_tipo_respuesta_reclamo,


    estatus:                      req.body.estatus,
      }
      
      Respuesta_reclamo.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'respuesta_reclamo actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Respuesta_reclamo.forge(conditions).fetch()
    .then(function(respuesta_reclamo){
      if(!respuesta_reclamo) return res.status(404).json({ error : true, data : { message : 'respuesta_reclamo no existe' } });

     Respuesta_reclamo.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'respuesta_reclamo eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}