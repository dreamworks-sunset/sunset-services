//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Item_logro = require('../models/item_logro');

exports.findDocuments = (req,res) => {
  
  Item_logro.forge().fetchAll(/*{withRelated:['detalle_sesion']}*/)
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    descripcion:         req.body.descripcion,
  }

  Item_logro.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'item_logro creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Item_logro.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'item_logro no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Item_logro.forge(conditions).fetch()
    .then(function(item_logro){
      if(!item_logro) return res.status(404).json({ error : true, data : { message : 'item_logro no existe' } });

      let updateData = {
        descripcion:         req.body.descripcion,
      }
      
      item_logro.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'item_logro actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Item_logro.forge(conditions).fetch()
    .then(function(item_logro){
      if(!item_logro) return res.status(404).json({ error : true, data : { message : 'item_logro no existe' } });

      item_logro.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'item_logro eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}