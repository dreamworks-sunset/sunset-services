//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Evaluacion = require('../models/evaluacion');

exports.findDocuments = (req,res) => {
  
  Evaluacion.forge().fetchAll(/*{withRelated:['evaluacion_sesion']}*/)
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    descripcion:         req.body.descripcion,
  }

  Evaluacion.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'evaluacion creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Evaluacion.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'evaluacion no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Evaluacion.forge(conditions).fetch()
    .then(function(evaluacion){
      if(!evaluacion) return res.status(404).json({ error : true, data : { message : 'evaluacion no existe' } });

      let updateData = {
        descripcion:         req.body.descripcion,
      }
      
      evaluacion.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'evaluacion actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Evaluacion.forge(conditions).fetch()
    .then(function(evaluacion){
      if(!evaluacion) return res.status(404).json({ error : true, data : { message : 'evaluacion no existe' } });

      evaluacion.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'evaluacion eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}