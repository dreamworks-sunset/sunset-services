//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Agenda = require('../models/agenda');

exports.findDocuments = (req,res) => {
  
  Agenda.forge().fetchAll({withRelated:['cita' , 'horario_empleado']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

 
  console.log(req.body);
  let newData = {
    id_cita:                           req.body.id_cita,
    id_horario_empleado:               req.body.id_horario_empleado,



 
     }  

  Agenda.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'agenda creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

 Agenda.forge(conditions).fetch({withRelated:['cita' , 'horario_empleado']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'agenda no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Agenda.forge(conditions).fetch()
    .then(function(agenda){
      if(!agenda) return res.status(404).json({ error : true, data : { message : 'agenda no existe' } });

      let updateData = {
    id_cita:                           req.body.id_cita,
    id_horario_empleado:               req.body.id_horario_empleado,
    estatus:                      req.body.estatus,
      }
      
      Agenda.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'agenda actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Agenda.forge(conditions).fetch()
    .then(function(agenda){
      if(!agenda) return res.status(404).json({ error : true, data : { message : 'agenda no existe' } });

     Agenda.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'agenda eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}