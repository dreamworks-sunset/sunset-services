//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Negocio = require('../models/negocio');
const cloudinary = require('../cloudinary'); //Importar para poder usar

exports.findDocuments = (req,res) => {
  
  Negocio.forge().fetchAll({withRelated:['empleado','red_social', 'filosofia', 'pregunta_frecuente','red_social_negocio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req, res) => {
  const logo = req.files.logo  //Se crea una constante que recibe el objeto de la imagen
  cloudinary.uploader.upload(logo.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion
    console.log(req.body);

    if (result.error) {
      return res.status(500).json({
          error: true,
          data: { message: result.error }
        });
    } 
    console.log(req.body);

      let newData = {

          nombre:          req.body.nombre,
          direccion:       req.body.direccion,
          rif:             req.body.rif,
          logo:            result.url,
          telefono_1:      req.body.telefono_1,
          telefono_2:      req.body.telefono_2,
          telefono_3:      req.body.telefono_3,
          correo:          req.body.correo,
          id_ciudad:       req.body.id_ciudad,
          id_sistema:      '1',
          url_direccion:   req.body.url_direccion
        }

    Negocio.forge(newData).save()
    .then(function(data){
      res.status(200).json({ error: false, data: { message: 'negocio creado' } });
    })
    .catch(function (err) {
      res.status(500).json({ error: true, data: {message: err.message} });
    });
 })
}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Negocio.forge(conditions).fetch({withRelated:['empleado', 'red_social', 'filosofia','pregunta_frecuente','red_social_negocio']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'negocio no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  if(req.body.cambio_estatus != 'true'){

            
          
    const logo = req.files.logo //Se crea una constante que recibe el objeto de la imagen
    console.log('logo =', logo);
    var resultado;
    if (logo.originalFilename != ''){
        cloudinary.uploader.upload(logo.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion
    
          if (result.error) {
            return res.status(500).json({
                error: true,
                data: { message: result.error }
              });
          }
          resultado = result; 
        }); //Fin imagen
      }
  let conditions = { id: req.params.id };

  Negocio.forge(conditions).fetch()
    .then(function(negocio){
      if(!negocio) return res.status(404).json({ error : true, data : { message : 'negocio no existe' } });
      var imagenCondicion = (resultado == undefined) ? negocio.logo : resultado.url;
      let updateData = {
        id:             req.params.id,
        nombre:          req.body.nombre,
        direccion:       req.body.direccion,
        rif:             req.body.rif,
        logo:            imagenCondicion,
        telefono_1:      req.body.telefono_1,
        telefono_2:      req.body.telefono_2,
        telefono_3:      req.body.telefono_3,
        correo:          req.body.correo,
        id_ciudad:       req.body.id_ciudad,
        id_sistema:      '1',
        url_direccion:  req.body.url_direccion,
        estatus:         req.body.estatus
      }
      console.log(req.body)
      negocio.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'negocio actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })
   
    } else
    {
      let conditions = { id: req.params.id };
        console.log(req.body);
        Negocio.forge(conditions).fetch()
          .then(function(negocio){
            if(!negocio) return res.status(404).json({ error : true, data : { message : 'negocio no existe' } });
      
            let updateData = {
                estatus:              req.body.estatus
            }
            
            negocio.save(updateData)
              .then(function(data){
      
                res.status(200).json({ error : false, data : { message : 'negocio actualizado'} });
              })
              .catch(function(err){
                res.status(500).json({ error : false, data : {message : err.message} });
              })
      
          })
          .catch(function(err){
                res.status(500).json({ error : false, data : {message : err.message} })
          })
          
        }
}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Negocio.forge(conditions).fetch()
    .then(function(negocio){
      if(!negocio) return res.status(404).json({ error : true, data : { message : 'negocio no existe' } });

      Negocio.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'negocio eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}