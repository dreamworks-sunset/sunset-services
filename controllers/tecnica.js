//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Tecnica = require('../models/tecnica');

exports.findDocuments = (req,res) => {
  
  Tecnica.forge().fetchAll({withRelated:['tipo_tecnica','servicio','sesion']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {
  console.log(req.body);
  let newData = {
    nombre:                         req.body.nombre,
    descripcion:                    req.body.descripcion,
    id_tipo_tecnica:                req.body.id_tipo_tecnica
  }

  Tecnica.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'tecnica creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });

  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Tecnica.forge(conditions).fetch({withRelated:['tipo_tecnica','tecnica','sesion']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'tecnica no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {
  let conditions = { id: req.params.id };

  Tecnica.forge(conditions).fetch()
    .then(function(tecnica){
      if(!tecnica) return res.status(404).json({ error : true, data : { message : 'tecnica no existe' } });

      let updateData = {
        nombre:                         req.body.nombre,
        descripcion:                    req.body.descripcion,
        id_tipo_tecnica:                req.body.id_tipo_tecnica,
        estatus:                        req.body.estatus
      }
      
      tecnica.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'tecnica actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Tecnica.forge(conditions).fetch()
    .then(function(tecnica){
      if(!tecnica) return res.status(404).json({ error : true, data : { message : 'tecnica no existe' } });

      tecnica.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'tecnica eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}