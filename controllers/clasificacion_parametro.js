//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const clasificacion_parametro = require('../models/clasificacion_parametro');

exports.findDocuments = (req,res) => {
  
  clasificacion_parametro.forge().fetchAll({withRelated:['valor_parametro']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    descripcion:         req.body.descripcion,
  }

  clasificacion_parametro.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'clasificacion_parametro creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  clasificacion_parametro.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'clasificacion_parametro no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  clasificacion_parametro.forge(conditions).fetch()
    .then(function(clasificacion_parametro){
      if(!clasificacion_parametro) return res.status(404).json({ error : true, data : { message : 'clasificacion_parametro no existe' } });

      let updateData = {
        descripcion:         req.body.descripcion,
      }
      
      clasificacion_parametro.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'clasificacion_parametro actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  clasificacion_parametro.forge(conditions).fetch()
    .then(function(clasificacion_parametro){
      if(!clasificacion_parametro) return res.status(404).json({ error : true, data : { message : 'clasificacion_parametro no existe' } });

      clasificacion_parametro.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'clasificacion_parametro eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}