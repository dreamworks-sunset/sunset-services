//----dependencias------  
'use strict'
const Vista_historias_medicas_clientes = require('../models/vista_historias_medicas_clientes');

exports.findDocuments = (req,res) => {
    
  Vista_historias_medicas_clientes.forge().fetchAll()
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}