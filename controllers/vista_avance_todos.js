//----dependencias------  
'use strict'
const Vista_avance_todos = require('../models/vista_avance_todos');

exports.findDocuments = (req,res) => {
    
  Vista_avance_todos.forge().fetchAll()
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}