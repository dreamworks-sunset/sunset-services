//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Filosofia = require('../models/filosofia');

exports.findDocuments = (req,res) => {
  
  Filosofia.forge().fetchAll({withRelated:['negocio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {
    mision:                     req.body.mision,
    vision:                     req.body.vision,
    objetivo_general:           req.body.objetivo_general,
    objetivo_especifico_1:      req.body.objetivo_especifico_1,
    objetivo_especifico_2:      req.body.objetivo_especifico_2,
    objetivo_especifico_3:      req.body.objetivo_especifico_3,
    objetivo_especifico_4:      req.body.objetivo_especifico_4,
    id_negocio:                 '1'
  }

  Filosofia.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'filosofia creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Filosofia.forge(conditions).fetch({withRelated:['negocio']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'filosofia no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Filosofia.forge(conditions).fetch()
    .then(function(filosofia){
      if(!filosofia) return res.status(404).json({ error : true, data : { message : 'filosofia no existe' } });

      let updateData = {
        id:                         req.params.id,
        mision:                     req.body.mision,
        vision:                     req.body.vision,
        objetivo_general:           req.body.objetivo_general,
        objetivo_especifico_1:      req.body.objetivo_especifico_1,
        objetivo_especifico_2:      req.body.objetivo_especifico_2,
        objetivo_especifico_3:      req.body.objetivo_especifico_3,
        objetivo_especifico_4:      req.body.objetivo_especifico_4,       
        estatus:                    req.body.estatus
      }
      
      filosofia.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'filosofia actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Filosofia.forge(conditions).fetch()
    .then(function(filosofia){
      if(!filosofia) return res.status(404).json({ error : true, data : { message : 'filosofia no existe' } });

      Filosofia.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'filosofia eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}