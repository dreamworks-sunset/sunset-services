//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Reclamo = require('../models/reclamo');

exports.findDocuments = (req,res) => {
  
  Reclamo.forge().fetchAll({withRelated:['tipo_reclamo' , 'orden_servicio.solicitud.servicio' , 'respuesta_reclamo','cliente' ]})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

 
  console.log(req.body);
  let newData = {
    id_orden_servicio:                     req.body.id_orden_servicio,
    id_cliente:                            req.body.id_cliente,
    id_tipo_reclamo:                       req.body.id_tipo_reclamo,
    descripcion:                           req.body.descripcion,

 
     }  

  Reclamo.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'reclamo creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Reclamo.forge(conditions).fetch({withRelated:['tipo_reclamo' , 'orden_servicio' , 'respuesta_reclamo','cliente' ]})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'reclamo no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Reclamo.forge(conditions).fetch()
    .then(function(reclamo){
      if(!reclamo) return res.status(404).json({ error : true, data : { message : 'reclamo no existe' } });

      let updateData = {
    id_orden_servicio:                     req.body.id_orden_servicio,
    id_cliente:                            req.body.id_cliente,
    id_tipo_reclamo:                       req.body.id_tipo_reclamo,
    descripcion:                           req.body.descripcion,
    estatus:                               req.body.estatus,
      }
      
      reclamo.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'reclamo actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Reclamo.forge(conditions).fetch()
    .then(function(reclamo){
      if(!reclamo) return res.status(404).json({ error : true, data : { message : 'reclamo no existe' } });

      Reclamo.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'reclamo eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}