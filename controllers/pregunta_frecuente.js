//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Pregunta_frecuente = require('../models/pregunta_frecuente');

exports.findDocuments = (req,res) => {
  
  Pregunta_frecuente.forge().fetchAll({withRelated:['negocio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {
    pregunta:         req.body.pregunta,
    respuesta:         req.body.respuesta,
    id_negocio:         req.body.id_negocio

   
    
  }

  Pregunta_frecuente.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'pregunta_frecuente creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Pregunta_frecuente.forge().fetchAll({withRelated:['negocio']})
      .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'pregunta_frecuente no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Pregunta_frecuente.forge(conditions).fetch()
    .then(function(pregunta_frecuente){
      if(!pregunta_frecuente) return res.status(404).json({ error : true, data : { message : 'pregunta_frecuente no existe' } });

      let updateData = {
            pregunta:         req.body.pregunta,
            respuesta:         req.body.respuesta,
            id_negocio:         req.body.id_negocio,
            estatus:        req.body.estatus
      }
      
      pregunta_frecuente.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'pregunta_frecuente actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Pregunta_frecuente.forge(conditions).fetch()
    .then(function(pregunta_frecuente){
      if(!pregunta_frecuente) return res.status(404).json({ error : true, data : { message : 'pregunta_frecuente no existe' } });

      pregunta_frecuente.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'pregunta_frecuente eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}