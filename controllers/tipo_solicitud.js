//---dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const tipo_solicitud = require('../models/tipo_solicitud');

exports.findDocuments = (req,res) => {
  
  tipo_solicitud.forge().fetchAll(/*{withRelated:['solicitud']}*/)
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    descripcion:         req.body.descripcion,
  }

  tipo_solicitud.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'tipo_solicitud creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_solicitud.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'tipo_solicitud no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_solicitud.forge(conditions).fetch()
    .then(function(tipo_solicitud){
      if(!tipo_solicitud) return res.status(404).json({ error : true, data : { message : 'tipo_solicitud no existe' } });

      let updateData = {
        descripcion:         req.body.descripcion,
      }
      
      tipo_solicitud.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'tipo_solicitud actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  tipo_solicitud.forge(conditions).fetch()
    .then(function(tipo_solicitud){
      if(!tipo_solicitud) return res.status(404).json({ error : true, data : { message : 'tipo_solicitud no existe' } });

      tipo_solicitud.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'tipo_solicitud eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}