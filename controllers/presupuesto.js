//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Presupuesto = require('../models/presupuesto');
const Solicitud = require('../models/solicitud');

exports.findDocuments = (req,res) => {
  
  Presupuesto.forge().fetchAll({withRelated:['solicitud', 'respuesta_presupuesto']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

 
  console.log(req.body);
  let newData = {
    id_solicitud:                     req.body.id_solicitud,
    monto:                        req.body.monto,

 
     }  

  Presupuesto.forge(newData).save()
  .then(function(data){
    Solicitud.forge({id: req.body.id_solicitud}).fetch()
    .then(function(solicitud){
      if(!solicitud) return res.status(404).json({ error : true, data : { message : 'solicitud no existe' } });

      let updateData = {     
        estatus:  2
      }
      
      solicitud.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'solicitud actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Presupuesto.forge(conditions).fetch({withRelated:['solicitud', 'respuesta_presupuesto']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'presupuesto no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Presupuesto.forge(conditions).fetch()
    .then(function(presupuesto){
      if(!presupuesto) return res.status(404).json({ error : true, data : { message : 'presupuesto no existe' } });

      let updateData = {
    id_solicitud:                     req.body.id_solicitud,
    monto:                        req.body.monto,
    estatus:                          req.body.estatus,
      }
      
      presupuesto.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'presupuesto actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Presupuesto.forge(conditions).fetch()
    .then(function(presupuesto){
      if(!presupuesto) return res.status(404).json({ error : true, data : { message : 'presupuesto no existe' } });

      Presupuesto.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'presupuesto eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}