//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Clasificacion_incidencia = require('../models/clasificacion_incidencia');

exports.findDocuments = (req,res) => {
  
  Clasificacion_incidencia.forge().fetchAll(/*{withRelated:['incidencia']}*/)
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    descripcion:         req.body.descripcion,
  }

  Clasificacion_incidencia.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'clasificacion_incidencia creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Clasificacion_incidencia.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'clasificacion_incidencia no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Clasificacion_incidencia.forge(conditions).fetch()
    .then(function(clasificacion_incidencia){
      if(!clasificacion_incidencia) return res.status(404).json({ error : true, data : { message : 'clasificacion_incidencia no existe' } });

      let updateData = {
        descripcion:         req.body.descripcion,
      }
      
      clasificacion_incidencia.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'clasificacion_incidencia actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Clasificacion_incidencia.forge(conditions).fetch()
    .then(function(clasificacion_incidencia){
      if(!clasificacion_incidencia) return res.status(404).json({ error : true, data : { message : 'clasificacion_incidencia no existe' } });

      clasificacion_incidencia.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'clasificacion_incidencia eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}