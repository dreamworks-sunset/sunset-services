//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Servicio = require('../models/servicio');
const cloudinary = require('../cloudinary'); // importar archivo para guardar imagen

exports.findDocuments = (req,res) => {
  
  Servicio.forge().fetchAll({withRelated:['tipo_servicio','solicitud','promocion','valor_parametro_servicio','valor_parametro_avance.parametro','tecnica','condicion_garantia','insumo','insumo_servicio','insumo_servicio.insumo','insumo_servicio.unidad','insumo_servicio.insumo.insumo_marca_proveedor.proveedor','insumo_servicio.insumo.insumo_marca_proveedor.marca','insumo_servicio.insumo.insumo_marca_proveedor']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  const imagen = req.files.imagen//Se crea una constante que recibe el objeto de la imagen
  console.log(req.body);
  cloudinary.uploader.upload(imagen.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion
    if (result.error) {
      return res.status(500).json({
          error: true,
          data: { message: result.error }
        });
      } 
    
    console.log(req.body);
    let newData = {
      nombre:                          req.body.nombre,
      descripcion:                     req.body.descripcion,
      id_tipo_servicio:                req.body.id_tipo_servicio,
      imagen:                          result.url,//La funcion anterior devuelve un resultado, y ese objeto se llama su atributo url y se obtiene la url donde fue guardada la imagen en cloudinary
      cantidad_sesiones:               req.body.cantidad_sesiones,
      precio_por_sesion:               req.body.precio_por_sesion,
      tiempo_por_sesion:               req.body.tiempo_por_sesion,
    }

    Servicio.forge(newData).save()
    .then(function(data){
      
      var valoresParametroServicio = JSON.parse('[' + req.body.valor_parametro_servicio + ']');
      var valoresParametroAvance = JSON.parse('[' + req.body.valor_parametro_avance + ']');
      var valoresTecnicaServicio = JSON.parse('[' + req.body.tecnica + ']');
      var valoresGarantiaServicio = JSON.parse('[' + req.body.condicion_garantia + ']');
      var valoresInsumoServicio = JSON.parse(req.body.insumo);
    
      if(valoresParametroServicio.length != 0) {				
  			data.valor_parametro_servicio().attach(valoresParametroServicio);
      }
      
      if(valoresParametroAvance.length != 0) {				
  			data.valor_parametro_avance().attach(valoresParametroAvance);
  		}
       if(valoresTecnicaServicio.length != 0) {       
        data.tecnica().attach(valoresTecnicaServicio);
      }
       if(valoresGarantiaServicio.length != 0) {       
        data.condicion_garantia().attach(valoresGarantiaServicio);
      }
      if(valoresInsumoServicio.length != 0) {       
        data.insumo().attach(valoresInsumoServicio);
      }

      res.status(200).json({ error: false, data: { message: 'servicio creado' } });
    })
    .catch(function (err) {
      res.status(500).json({ error: true, data: {message: err.message} });
      console.log(err);

    });
  })
}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Servicio.forge(conditions).fetch({withRelated:[
    'tipo_servicio','solicitud','promocion','valor_parametro_servicio','valor_parametro_avance.parametro','tecnica','condicion_garantia','insumo','insumo_servicio','insumo_servicio.unidad','insumo_servicio.insumo.insumo_marca_proveedor.proveedor','insumo_servicio.insumo.insumo_marca_proveedor.marca','insumo_servicio.insumo.insumo_marca_proveedor']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'servicio no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {
   console.log(req.body);
  if(req.body.cambio_estatus != 'true') { 
   
    const imagen = req.files.imagen//Se crea una constante que recibe el objeto de la imagen
    console.log('imagen =', imagen);
    var resultado;
    if (imagen.originalFilename != ''){
    cloudinary.uploader.upload(imagen.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion
      if (result.error) {
        return res.status(500).json({
            error: true,
            data: { message: result.error }
          });
        } 
        resultado = result;
  });
}
        let conditions = { id: req.params.id };
   
            Servicio.forge(conditions).fetch()
              .then(function(servicio){
                if(!servicio) return res.status(404).json({ error : true, data : { message : 'servicio no existe' } });
                var imagenCondicion = (resultado==undefined) ? servicio.imagen : resultado.url;
                let updateData = {
                  nombre:                          req.body.nombre,
                  descripcion:                     req.body.descripcion,
                  id_tipo_servicio:                req.body.id_tipo_servicio,
                  imagen:                          imagenCondicion,//La funcion anterior devuelve un resultado, y ese objeto se llama su atributo url y se obtiene la url donde fue guardada la imagen en cloudinary
                  cantidad_sesiones:               req.body.cantidad_sesiones,
                  precio_por_sesion:               req.body.precio_por_sesion,
                  tiempo_por_sesion:               req.body.tiempo_por_sesion,
                  estatus:                         req.body.estatus,
                }
               console.log(req.body)
              servicio.save(updateData)
                .then(function(data){
                  if(req.body.cambio_estatus != 'true') { 

                    var valoresParametroServicioPorEliminar = JSON.parse('[' + req.body.valor_parametro_servicio_eliminar + ']');
                    var valoresParametroServicio = JSON.parse('[' + req.body.valor_parametro_servicio + ']');
                    var valoresParametroAvancePorEliminar = JSON.parse('[' + req.body.valor_parametro_avance_eliminar + ']');
                    var valoresParametroAvance = JSON.parse('[' + req.body.valor_parametro_avance + ']');
                    var valoresTecnicaServicioPorEliminar = JSON.parse('[' + req.body.tecnica_eliminar + ']');
                    var valoresTecnicaServicio = JSON.parse('[' + req.body.tecnica + ']');
                    var valoresGarantiaServicioPorEliminar = JSON.parse('[' + req.body.condicion_garantia_eliminar + ']');
                    var valoresGarantiaServicio= JSON.parse('[' + req.body.condicion_garantia + ']');
                    var valoresInsumoServicio = JSON.parse(req.body.insumo);
                    var valoresInsumoServicioPorEliminar = JSON.parse(req.body.insumo_eliminar);

                    //Eliminamos los valores parámetros que ya no se asocian con el servicio
                    if(valoresParametroServicioPorEliminar.length != 0) {				
          			      data.valor_parametro_servicio().detach(valoresParametroServicioPorEliminar);
          		      }

                    //Añadimos los nuevos valores parámetros que se asocian con el servicio
                    if(valoresParametroServicio.length != 0) {				
          			      data.valor_parametro_servicio().attach(valoresParametroServicio);
                    }
                    
                    //Eliminamos los valores parámetros del avance que ya no se asocian con el servicio
                    if(valoresParametroAvancePorEliminar.length != 0) {				
          			      data.valor_parametro_avance().detach(valoresParametroAvancePorEliminar);
          		      }

                    //Añadimos los nuevos valores parámetros del avance que se asocian con el servicio
                    if(valoresParametroAvance.length != 0) {				
          			      data.valor_parametro_avance().attach(valoresParametroAvance);
                    }
                              
                    //Eliminamos las tecnicas que ya no se asocian con el servicio
                    if(valoresTecnicaServicioPorEliminar.length != 0) {       
                      data.tecnica().detach(valoresTecnicaServicioPorEliminar);
                    }

                    //Añadimos las tecnicas que se asocian con el servicio
                    if(valoresTecnicaServicio.length != 0) {        
                      data.tecnica().attach(valoresTecnicaServicio);
                    }

                    //Eliminamos las condiciones de garantia que ya no se asocian con el servicio
                    if(valoresGarantiaServicioPorEliminar.length != 0) {       
                      data.condicion_garantia().detach(valoresGarantiaServicioPorEliminar);
                    }

                    //Añadimos las condiciones de garantia que se asocian con el servicio
                    if(valoresGarantiaServicio.length != 0) {        
                      data.condicion_garantia().attach(valoresGarantiaServicio);
                    }
          
                    //Eliminamos los insumos que ya no se asocian con el servicio
                    if(valoresInsumoServicioPorEliminar.length != 0) {       
                      data.insumo().detach(valoresInsumoServicioPorEliminar);
                    }

                    //Añadimos los insumos que se asocian con el servicio
                    if(valoresInsumoServicio.length != 0) {        
                      data.insumo().attach(valoresInsumoServicio); 
                    }
                  }
                  res.status(200).json({ error : false, data : { message : 'servicio actualizado'} });
                })
                .catch(function(err){
                  res.status(500).json({ error : false, data : {message : err.message} });

                })

            })
            .catch(function(err){
                  res.status(500).json({ error : false, data : {message : err.message} })
            })
  // fin imagen
  } else {
    let conditions = { id: req.params.id };
    console.log(req.body);
    Servicio.forge(conditions).fetch()
      .then(function(servicio){
        if(!servicio) return res.status(404).json({ error : true, data : { message : 'servicio no existe' } });

        let updateData = {
          estatus:                         req.body.estatus,
        }
      
      servicio.save(updateData)
        .then(function(data){
          
          res.status(200).json({ error : false, data : { message : 'servicio actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });

        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })
  }
}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Servicio.forge(conditions).fetch()
    .then(function(servicio){
      if(!servicio) return res.status(404).json({ error : true, data : { message : 'servicio no existe' } });

      servicio.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'servicio eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}