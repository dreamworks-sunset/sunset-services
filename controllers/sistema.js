//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Sistema = require('../models/sistema');
const cloudinary = require('../cloudinary'); //Importar para poder usar

exports.findDocuments = (req,res) => {
  
  Sistema.forge().fetchAll({withRelated:['negocio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

    const logo = req.files.logo//Se crea una constante que recibe el objeto de la imagen
    cloudinary.uploader.upload(logo.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion

      if (result.error) {
        return res.status(500).json({
            error: true,
            data: { message: result.error }
          });
      } 

  let newData = {
    nombre:          req.body.nombre,
    logo:                 result.url,//La funcion anterior devuelve un resultado, y ese objeto se llama su atributo url y se obtiene la url donde fue guardada la imagen en cloudinary
  }

  Sistema.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'sistema creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });
   })

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

Sistema.forge(conditions).fetch({withRelated:['negocio']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'sistema no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {
if(req.body.cambio_estatus != 'true'){

            
          
const logo = req.files.logo//Se crea una constante que recibe el objeto de la imagen
console.log(req.body);    
cloudinary.uploader.upload(logo.path, function(result) { //Se ejecuta esta funcion de cloudinary enviando por parámetro la ruta donde se encuentra almacenada la imagen en la pc y una funcion

      if (result.error) {
        return res.status(500).json({
            error: true,
            data: { message: result.error }
          });
      } 


  let conditions = { id: req.params.id };

  Sistema.forge(conditions).fetch()
    .then(function(sistema){
      if(!sistema) return res.status(404).json({ error : true, data : { message : 'sistema no existe' } });

      let updateData = {
      id:                   req.params.id,
        nombre:          req.body.nombre,
       logo:                 result.url,//La funcion anterior devuelve un resultado, y ese objeto se llama su atributo url y se obtiene la url donde fue guardada la imagen en cloudinary      
        estatus:              req.body.estatus
      }
      
      sistema.save(updateData)
        .then(function(data){

          res.status(200).json({ error : false, data : { message : 'sistema actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })
    });
    //Fin imagen
    } else
    {
let conditions = { id: req.params.id };

  Sistema.forge(conditions).fetch()
    .then(function(sistema){
      if(!sistema) return res.status(404).json({ error : true, data : { message : 'sistema no existe' } });

      let updateData = {
          estatus:              req.body.estatus
      }
      
      sistema.save(updateData)
        .then(function(data){

          res.status(200).json({ error : false, data : { message : 'sistema actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })
    }

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Sistema.forge(conditions).fetch()
    .then(function(sistema){
      if(!sistema) return res.status(404).json({ error : true, data : { message : 'sistema no existe' } });

     Sistema.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'sistema eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}