//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Evaluacion_sesion = require('../models/evaluacion_sesion');

exports.findDocuments = (req,res) => {
  
  Evaluacion_sesion.forge().fetchAll({withRelated: ['sesion.cita.orden_servicio.solicitud.servicio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {
  console.log(req.body);
  let newData = {  
    id_criterio:         req.body.id_criterio,
    id_evaluacion:       req.body.id_evaluacion,
    id_sesion:           req.body.id_sesion
  }

  Evaluacion_sesion.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'evaluacion_sesion creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}