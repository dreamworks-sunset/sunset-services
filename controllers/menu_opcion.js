//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Menu_opcion = require('../models/menu_opcion');

exports.findDocuments = (req,res) => {
  
  Menu_opcion.forge().fetchAll({withRelated:['menu_opcion_padre','menu_opcion_padre.menu_opcion_padre','menu_opcion','menu_opcion.menu_opcion','rol']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

 
  console.log(req.body);
  let newData = {
    id_menu_opcion:                           req.body.id_menu_opcion,
    funcion:                                  req.body.funcion,
  }  

  Menu_opcion.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'menu_opcion creada' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

Menu_opcion.forge(conditions).fetch({withRelated:['menu_opcion_padre','menu_opcion_padre.menu_opcion_padre','menu_opcion','menu_opcion.menu_opcion','rol']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'menu_opcion no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Menu_opcion.forge(conditions).fetch()
    .then(function(menu_opcion){
      if(!menu_opcion) return res.status(404).json({ error : true, data : { message : 'menu_opcion no existe' } });

      let updateData = {
    id_menu_opcion:                           req.body.id_menu_opcion,
    funcion:                                  req.body.funcion,
    estatus:                                  req.body.estatus,
      }
      
      menu_opcion.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'menu_opcion actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Menu_opcion.forge(conditions).fetch()
    .then(function(menu_opcion){
      if(!menu_opcion) return res.status(404).json({ error : true, data : { message : 'menu_opcion no existe' } });

     Menu_opcion.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'menu_opcion eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}