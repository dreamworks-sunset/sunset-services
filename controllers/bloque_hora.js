//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Bloque_hora = require('../models/bloque_hora');

exports.findDocuments = (req,res) => {
  
  Bloque_hora.forge().fetchAll({withRelated:['horario','solicitud']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {
    hora_inicio:         req.body.hora_inicio,
    hora_fin:    req.body.hora_fin
  }

  Bloque_hora.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'bloque_hora creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Bloque_hora.forge(conditions).fetch({withRelated:['horario','solicitud']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'bloque_hora no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Bloque_hora.forge(conditions).fetch()
    .then(function(bloque_hora){
      if(!bloque_hora) return res.status(404).json({ error : true, data : { message : 'bloque_hora no existe' } });

      let updateData = {
        id:             req.params.id,
        hora_inicio:         req.body.hora_inicio,
        hora_fin:    req.body.hora_fin,       
        estatus:                    req.body.estatus
      }
      
      bloque_hora.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'bloque_hora actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Bloque_hora.forge(conditions).fetch()
    .then(function(bloque_hora){
      if(!bloque_hora) return res.status(404).json({ error : true, data : { message : 'bloque_hora no existe' } });

      Bloque_hora.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'bloque_hora eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}