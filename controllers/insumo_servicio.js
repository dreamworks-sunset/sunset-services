//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Insumo_servicio = require('../models/insumo_servicio');

exports.findDocuments = (req,res) => {
  
  Insumo_servicio.forge().fetchAll({withRelated:['insumo','servicio','unidad']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {
  console.log(req.body);
  let newData = {
    id_insumo:      req.body.id_insumo,
    id_servicio:    req.body.id_servicio,
    id_unidad:      req.body.id_unidad
  }

  Insumo_servicio.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'insumo_servicio creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });

  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Insumo_servicio.forge(conditions).fetch({withRelated:['insumo','servicio','unidad']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'insumo_servicio no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {
  let conditions = { id: req.params.id };

  Insumo_servicio.forge(conditions).fetch()
    .then(function(insumo_servicio){
      if(!insumo_servicio) return res.status(404).json({ error : true, data : { message : 'insumo_servicio no existe' } });

      let updateData = {
        id_insumo:      req.body.id_insumo,
        id_servicio:    req.body.id_servicio,
        id_unidad:      req.body.id_unidad,
        estatus:        req.body.estatus
      }
      
      insumo_servicio.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'insumo_servicio actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Insumo_servicio.forge(conditions).fetch()
    .then(function(insumo_servicio){
      if(!insumo_servicio) return res.status(404).json({ error : true, data : { message : 'insumo_servicio no existe' } });

      insumo_servicio.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'insumo_servicio eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}