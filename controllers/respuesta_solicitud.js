//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Respuesta_solicitud = require('../models/respuesta_solicitud');
const Solicitud = require('../models/solicitud');

exports.findDocuments = (req,res) => {
  
  Respuesta_solicitud.forge().fetchAll({withRelated:['tipo_respuesta_solicitud' , 'solicitud']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {  
    id_solicitud:                           req.body.id_solicitud,
    id_tipo_respuesta_solicitud:            req.body.id_tipo_respuesta_solicitud,
    contenido:                              req.body.contenido
  }

  Respuesta_solicitud.forge(newData).save()
  .then(function(data){
    var id_solicitud = data.get('id_solicitud');
    Solicitud.forge({id: id_solicitud}).fetch()
    .then(function(solicitud) {
      let updateData = {
        estatus: 2
      }
      solicitud.save(updateData)
      .then(function(data){
          console.log('Solicitud respondida');
        })
      .catch(function(err){
        res.status(500).json({ error : true, data : {message : err.message} })
      });
    })
    .catch();
    res.status(200).json({ error: false, data: { message: 'respuesta_comentario creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}





exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Respuesta_solicitud.forge(conditions).fetch()
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'respuesta_solicitud no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
console.log(req.body);
  Respuesta_solicitud.forge(conditions).fetch()
    .then(function(respuesta_solicitud){
      if(!respuesta_solicitud) return res.status(404).json({ error : true, data : { message : 'respuesta_solicitud no existe' } });

      let updateData = {
    id_solicitud:                           req.body.id_solicitud,
    id_tipo_respuesta_solicitud:            req.body.id_tipo_respuesta_solicitud,
    contenido:                              req.body.contenido,

    estatus:                      req.body.estatus,
      }
      
      respuesta_solicitud.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'respuesta_solicitud actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Respuesta_solicitud.forge(conditions).fetch()
    .then(function(respuesta_solicitud){
      if(!respuesta_solicitud) return res.status(404).json({ error : true, data : { message : 'respuesta_solicitud no existe' } });

     Respuesta_solicitud.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'respuesta_solicitud eliminada'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}