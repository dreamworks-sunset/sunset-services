//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Insumo_marca_proveedor = require('../models/insumo_marca_proveedor');

exports.findDocuments = (req,res) => {
  
  Insumo_marca_proveedor.forge().fetchAll({withRelated:['insumo.insumo_servicio.unidad','marca','proveedor','sesion','insumo_utilizado','incidencia']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {
  console.log(req.body);
  let newData = {
    id_insumo:      req.body.id_insumo,
    id_marca:       req.body.id_marca,
    id_proveedor:   req.body.id_proveedor
  }

  Insumo_marca_proveedor.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'insumo_marca_proveedor creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });

  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Insumo_marca_proveedor.forge(conditions).fetch({withRelated:['insumo.insumo_servicio.unidad','marca','proveedor','sesion','insumo_utilizado','incidencia']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'insumo_marca_proveedor no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {
  let conditions = { id: req.params.id };

  Insumo_marca_proveedor.forge(conditions).fetch()
    .then(function(insumo_marca_proveedor){
      if(!insumo_marca_proveedor) return res.status(404).json({ error : true, data : { message : 'insumo_marca_proveedor no existe' } });

      let updateData = {
        id_insumo:      req.body.id_insumo,
        id_marca:       req.body.id_marca,
        id_proveedor:   req.body.id_proveedor,
        estatus:        req.body.estatus
      }
      
      insumo_marca_proveedor.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'insumo_marca_proveedor actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Insumo_marca_proveedor.forge(conditions).fetch()
    .then(function(insumo_marca_proveedor){
      if(!insumo_marca_proveedor) return res.status(404).json({ error : true, data : { message : 'insumo_marca_proveedor no existe' } });

      insumo_marca_proveedor.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'insumo_marca_proveedor eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}