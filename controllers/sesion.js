//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Sesion = require('../models/sesion');
const Cita = require('../models/cita');
const Incidencia = require('../models/incidencia');

exports.findDocuments = (req,res) => {
  
    Sesion.forge().fetchAll({withRelated:['cita.agenda.horario_empleado.empleado', 'incidencia','evaluacion','criterio','insumo','insumo_utilizado.insumo_marca_proveedor.marca','insumo_utilizado.insumo_marca_proveedor.proveedor','insumo_utilizado.insumo_marca_proveedor.insumo','tecnica','item_logro','item_detalle','cita.orden_servicio.solicitud.cliente','cita.orden_servicio.solicitud.servicio.valor_parametro_avance.parametro','cita.orden_servicio.solicitud.promocion']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  console.log(req.body);

  Cita.forge({id: req.body.id_cita}).fetch()
  .then(function(cita) {
      if(!cita) return res.status(404).json({ error : true, data : { message : 'cita no existe' } });
      
      var estatus = (req.body.hay_incidencia != 'true') ? req.body.estatus : 3;

      let updateData = {
        estatus: estatus
      }

      cita.save(updateData)
      .then(function(data) {
        let newData = {
          id_cita:              req.body.id_cita,
          numero_sesion:        req.body.numero_sesion,
        }

        Sesion.forge(newData).save()
        .then(function(data){
            if(req.body.hay_incidencia != 'true'){
              console.log('No hay incidencia');
              var valoresInsumoDetalle = JSON.parse(req.body.insumo);
              var valoresTecnicaDetalle = JSON.parse(req.body.tecnica);
              var valoresItemLogro = JSON.parse(req.body.item_logro);

              if(valoresInsumoDetalle.length != 0) {       
                data.insumo().attach(valoresInsumoDetalle);
              }
              if(valoresTecnicaDetalle.length != 0) {       
                data.tecnica().attach(valoresTecnicaDetalle);
              }
              if(valoresItemLogro.length != 0) {       
                data.item_logro().attach(valoresItemLogro);
              }
             
              if(req.body.id_empleado != undefined) {
                let newCita = {
                  id_orden_servicio:  req.body.id_orden_servicio,
                  id_empleado:        req.body.id_empleado,
                  fecha:              req.body.fecha
                }

                Cita.forge(newCita).save()
                .then(function(cita){
                  res.status(200).json({ error: false, data: data.toJSON(), message: 'cita creado' });
                })
                .catch(function (err) {
                  res.status(500).json({ error: true, data: {message: err.message} });
                });
              } else {
                res.status(200).json({ error: false, data: data.toJSON(), message: 'sesion creada' });
              }
            } else {
              var valoresIncidencia = JSON.parse(req.body.incidencia);
              console.log(valoresIncidencia);
               if(valoresIncidencia.id_clasificacion_incidencia != undefined) {
                let newIncidencia = {
                  id_tipo_incidencia:                 valoresIncidencia.id_tipo_incidencia,
                  id_clasificacion_incidencia:        valoresIncidencia.id_clasificacion_incidencia,
                  contenido:                          valoresIncidencia.contenido,
                  id_sesion:                          data.get('id')
                }
                
                Incidencia.forge(newIncidencia).save()
                .then(function(incidencia){
                  res.status(200).json({ error: false, data: data.toJSON(), message: 'incidencia creado' });
                })
                .catch(function (err) {
                  res.status(500).json({ error: true, data: {message: err.message} });
                });
              }
              if(req.body.id_empleado != undefined) {
                let newCita = {
                  id_orden_servicio:  req.body.id_orden_servicio,
                  id_empleado:        req.body.id_empleado,
                  fecha:              req.body.fecha
                }

                Cita.forge(newCita).save()
                .then(function(cita){
                  res.status(200).json({ error: false, data: data.toJSON(), message: 'cita creado' });
                })
                .catch(function (err) {
                  res.status(500).json({ error: true, data: {message: err.message} });
                });
              } else {
                res.status(200).json({ error: false, data: data.toJSON(), message: 'sesion creada' });
              }
              
            }
        })
        .catch(function (err) {
          res.status(500).json({ error: true, data: {message: err.message} });
        });
      })
      .catch(function(err) {
        res.status(500).json({ error: true, data: {message: err.message} });
      });
    })
    .catch(function(err) {
      res.status(500).json({ error: true, data: {message: err.message} });
    });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Sesion.forge(conditions).fetch({withRelated:['cita.agenda.horario_empleado.empleado','incidencia','evaluacion','criterio','insumo','insumo_utilizado.insumo_marca_proveedor.marca','insumo_utilizado.insumo_marca_proveedor.proveedor','insumo_utilizado.insumo_marca_proveedor.insumo','tecnica','item_logro','item_detalle','cita.orden_servicio.solicitud.cliente','cita.orden_servicio.solicitud.servicio.valor_parametro_avance.parametro','cita.orden_servicio.solicitud.promocion']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'sesion no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };
  console.log(req.body)
  Sesion.forge(conditions).fetch()
    .then(function(sesion){
      if(!sesion) return res.status(404).json({ error : true, data : { message : 'sesion no existe' } });

      let updateData = {
        id:                   req.params.id,
        id_cita:              req.body.id_cita,
        numero_sesion:        req.body.numero_sesion,      
        estatus:              req.body.estatus
      }
      
      sesion.save(updateData)
        .then(function(data){
          var valoresAvance = JSON.parse(req.body.avance);

            if(valoresAvance.length != 0) {       
              data.valor_parametro().attach(valoresAvance);
            }

          res.status(200).json({ error : false, data : { message : 'sesion actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Sesion.forge(conditions).fetch()
    .then(function(sesion){
      if(!sesion) return res.status(404).json({ error : true, data : { message : 'sesion no existe' } });

      Sesion.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'sesion eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}