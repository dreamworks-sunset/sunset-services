//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Imagen = require('../models/imagen');

exports.findDocuments = (req,res) => {
  
    Imagen.forge().fetchAll({withRelated:['sistema','seccion_web_imagen','promocion']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {

    id_seccion_web:       req.body.id_seccion_web,
    imagen:               req.body.imagen,
    nombre:               req.body.descripcion,
    id_sistema:           req.body.id_sistema,  
  }

  Imagen.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'imagen creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Imagen.forge(conditions).fetch({withRelated:['sistema','seccion_web','promocion']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'imagen no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Imagen.forge(conditions).fetch()
    .then(function(imagen){
      if(!imagen) return res.status(404).json({ error : true, data : { message : 'imagen no existe' } });

      let updateData = {
        id:                    req.params.id,
        id_seccion_web:       req.body.id_seccion_web,
        imagen:               req.body.imagen,
        nombre:               req.body.descripcion,
        fecha_creacion:         req.body.fecha_creacion,       
        estatus:               req.body.estatus,
        id_sistema:           req.body.id_sistema,
      }
      
      imagen.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'imagen actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Imagen.forge(conditions).fetch()
    .then(function(imagen){
      if(!imagen) return res.status(404).json({ error : true, data : { message : 'imagen no existe' } });

      Imagen.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'imagen eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}