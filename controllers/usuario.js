//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Usuario = require('../models/usuario');
const ViewCliente   = require('../models/cliente');
const Empleado= require('../models/empleado');

exports.findDocuments = (req,res) => {
  
  Usuario.forge().query(function(qb) {
    qb.whereNull('id_rol').orWhere('id_rol', '<>', 0);
  }).fetchAll({
    withRelated: [
      'rol.tipo_rol', 
      'cliente', 
      'empleado', 
      'notificacion'
    ],
    columns: ['correo', 'id', 'id_rol', 'ultimo_acceso', 'estatus', 'fecha_creacion'],
  })
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  //encriptado de contraseña
  let salt = bcrypt.genSaltSync(12);
  let hash = bcrypt.hashSync(req.body.contrasenia, salt);

  let newData = {
    id_rol:         req.body.id_rol,
    correo:         req.body.correo,
    contrasenia:    hash
  }

  Usuario.forge(newData).save()
  .then(function(data){

     let conditions = { id: req.body.id_empleado };

     Empleado.forge(conditions).fetch()
    .then(function(empleado){
      if(!empleado) return res.status(404).json({ error : true, data : { message : 'empleado no existe' } });

      let updateData = {
        id_usuario:     data.get('id')

      }
      
      empleado.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'empleado actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function (err) {
      res.status(500).json({ error: true, data: {message: err.message} });
    });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Usuario.forge(conditions).fetch({
    withRelated: [
      'rol.tipo_rol', 
      'rol.menu_opcion.menu_opcion_padre.menu_opcion_padre',
      'cliente',
      'empleado',
      'notificacion'
    ],
    columns: ['correo', 'id', 'id_rol', 'ultimo_acceso', 'estatus', 'fecha_creacion'],
  })
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'usuario no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Usuario.forge(conditions).fetch()
    .then(function(usuario){
      if(!usuario) return res.status(404).json({ error : true, data : { message : 'usuario no existe' } });

      //encriptado de contraseña
      let salt = bcrypt.genSaltSync(12);
      let contrasenna = usuario.get('contrasenia'); 
      if (req.body.contrasenia != undefined )
      {
          contrasenna = bcrypt.hashSync(req.body.contrasenia, salt);
      } 

      


      let updateData = {
        id_rol:         req.body.id_rol,
        correo:         req.body.correo,
        contrasenia:    contrasenna,
        estatus:        req.body.estatus
      }
      
      usuario.save(updateData)
        .then(function(data){


          res.status(200).json({ error : false, data : { message : 'usuario actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Usuario.forge(conditions).fetch()
    .then(function(usuario){
      if(!usuario) return res.status(404).json({ error : true, data : { message : 'usuario no existe' } });

      usuario.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'usuario eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.signIn = (req, res) => {
  if(!req.body.correo)
    return res.status(400).json({ error: true, data: { mensaje: 'Faltan parametros en el body' } });
  
  let credenciales = {
    correo: req.body.correo ? req.body.correo.toLowerCase() : null,
  }
  Usuario.query(function(qb) { 
    qb.where('correo', credenciales.correo);
    qb.where('estatus', 1); 
  })
  .fetch()
  .then(function(usuario){
    if(!usuario)
      return res.status(404).json({ 
        error: true, 
        data: { mensaje: 'Nombre de usuario o Correo inválido' } 
      });
        
    const esContrasenia = bcrypt.compareSync(req.body.contrasenia, usuario.get('contrasenia'));
    if(esContrasenia) {
      usuario.save({ultimo_acceso: new Date()})
      .then(function(usr){
        ViewCliente.forge({ id_usuario: usuario.get('id') })
        .fetch({ columns: ['id', 'cedula', 'nombres', 'apellidos', 
                  'telefono', 'direccion', 
                  'fecha_nacimiento', 'imagen'] })
        .then(function(cliente) {
          if(!cliente) 
            return res.status(404).json({ 
              error: true, 
              data: { mensaje: 'Cliente no encontrado' } 
            });
          const data = { 
            mensaje: 'Inicio de sesión exitoso',
            //token: service.createToken(usuario),
            cliente: cliente
          }
          return res.status(200).json({ 
            error: false, 
            data: data
          });
        })
        .catch(function(err){
          return res.status(500).json({ 
            error: false, 
            data: { mensaje: err.message } 
          })
        });
      })
      .catch(function(err){
        res.status(500).json({ error : false, data : {message : err.message} });
      });
    }
    else {
      return res.status(404).json({ 
        error: true, 
        data: { mensaje: 'contraseña inválida' } 
      });
    }
  })
  .catch(function(err){
    console.log(err.mensaje);
    return res.status(500).json({ 
      error: true, 
      data: { mensaje: err.message } 
    });
  })
}

exports.signInIntranet = (req, res) => {
  let conditions = { correo: req.body.correo };

  Usuario.forge(conditions).fetch({
    withRelated: [
      'rol.tipo_rol', 
      'rol.menu_opcion.menu_opcion_padre.menu_opcion_padre',
      'cliente',
      'empleado',
      'notificacion'
    ],
  })
	.then(function(usuario){
		if(!usuario) return res.status(404).send({error: true, data: { message:"Correo o contraseña incorrectos" } });
        
        let isPassword = bcrypt.compareSync(req.body.contrasenia, usuario.get("contrasenia"))
		if(isPassword){
      usuario.save({ultimo_acceso: new Date()})
      .then(function(data){
        data.set('contrasenia', undefined);
        res.status(200).send({ error: false, data: data.toJSON() })
      })
      .catch(function(err){
        res.status(500).json({ error : false, data : {message : err.message} });
      })
		}else{
			res.status(404).send({ error: true, data: {message: "La contraseña es incorrecta"} })
		}
	})
	.catch(function(err){
		res.status(500).send({ error: true, data: { message: err.message }})
	})
}