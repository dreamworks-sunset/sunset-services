//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Eventualidad = require('../models/eventualidad');

exports.findDocuments = (req,res) => {
  
    Eventualidad.forge().fetchAll({withRelated:['negocio']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {
    nombre:         req.body.nombre,
    descripcion:    req.body.descripcion,
    fecha_inicio:   req.body.fecha_inicio,
    fecha_fin:      req.body.fecha_fin,
    id_negocio:     req.body.id_negocio,
  }

  Eventualidad.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'eventualidad creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Eventualidad.forge(conditions).fetch({withRelated:['usuario']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'eventualidad no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Eventualidad.forge(conditions).fetch()
    .then(function(eventualidad){
      if(!eventualidad) return res.status(404).json({ error : true, data : { message : 'eventualidad no existe' } });

      let updateData = {
        id:             req.params.id,
        nombre:         req.body.nombre,
        descripcion:    req.body.descripcion,
        fecha_inicio:   req.body.fecha_inicio,
        fecha_fin:      req.body.fecha_fin,
        id_negocio:     req.body.id_negocio,
        estatus:        req.body.estatus,
      }
      
      eventualidad.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'eventualidad actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Eventualidad.forge(conditions).fetch()
    .then(function(eventualidad){
      if(!eventualidad) return res.status(404).json({ error : true, data : { message : 'eventualidad no existe' } });

      Eventualidad.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'eventualidad eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}