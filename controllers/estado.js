//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const estado = require('../models/estado');

exports.findDocuments = (req,res) => {
  
  estado.forge().fetchAll({withRelated:['ciudad','pais']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  //encriptado de contraseña
  let salt = bcrypt.genSaltSync(12);
  let hash = bcrypt.hashSync(req.body.contrasenia, salt);

  let newData = {  
    id_pais:           req.body.id_pais,
    descripcion:         req.body.descripcion,
        estatus:         1,
  }

  estado.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'estado creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  estado.forge(conditions).fetch({withRelated:['ciudad','pais']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'estado no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  estado.forge(conditions).fetch()
    .then(function(estado){
      if(!estado) return res.status(404).json({ error : true, data : { message : 'estado no existe' } });

      //encriptado de contraseña
      let salt = bcrypt.genSaltSync(12);
      let hash = bcrypt.hashSync(req.body.contrasenia, salt);

      let updateData = {
         id_pais:            req.body.id_pais,
        descripcion:         req.body.descripcion,
        estatus:             req.body.estatus,
      }
      
      estado.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'estado actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  estado.forge(conditions).fetch()
    .then(function(estado){
      if(!estado) return res.status(404).json({ error : true, data : { message : 'estado no existe' } });

      estado.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'estado eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}