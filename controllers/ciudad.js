//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const ciudad = require('../models/ciudad');

exports.findDocuments = (req,res) => {
  
  ciudad.forge().fetchAll({withRelated:['estado']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  //encriptado de contraseña
  let salt = bcrypt.genSaltSync(12);
  let hash = bcrypt.hashSync(req.body.contrasenia, salt);

  let newData = {  
     id_estado:           req.body.id_estado,
    descripcion:         req.body.descripcion,
        estatus:         1,
  }

  ciudad.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'ciudad creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  ciudad.forge(conditions).fetch({withRelated:['estado']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'ciudad no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  ciudad.forge(conditions).fetch()
    .then(function(ciudad){
      if(!ciudad) return res.status(404).json({ error : true, data : { message : 'ciudad no existe' } });

      //encriptado de contraseña
      let salt = bcrypt.genSaltSync(12);
      let hash = bcrypt.hashSync(req.body.contrasenia, salt);

      let updateData = {
        id_estado:           req.body.id_estado,
        descripcion:         req.body.descripcion,
        estatus:        req.body.estatus,
      }
      
      ciudad.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'ciudad actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  ciudad.forge(conditions).fetch()
    .then(function(ciudad){
      if(!ciudad) return res.status(404).json({ error : true, data : { message : 'ciudad no existe' } });

      ciudad.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'ciudad eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}