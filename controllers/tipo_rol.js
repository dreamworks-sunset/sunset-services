//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Tipo_rol = require('../models/tipo_rol');

exports.findDocuments = (req,res) => {
  
    Tipo_rol.forge().where('id', '<>', 0).fetchAll({
      withRelated: [
          'rol.menu_opcion.menu_opcion_padre.menu_opcion_padre',
          'rol.usuario.empleado.tipo_servicio',
          'rol.usuario.cliente'
        ]
    })
    .then(function(data){
        res.status(200).json({ error : false, data : data.toJSON() });
    })
    .catch(function (err) {
        res.status(500).json({ error: true, data: {message: err.message} });
    });

}

exports.createDocument = (req,res) => {

  let newData = {
    descripcion:    req.body.descripcion
  }

  Tipo_rol.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'tipo_rol creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Tipo_rol.forge(conditions).where('id', '<>', 0).fetch({
      withRelated: [
          'rol.menu_opcion.menu_opcion_padre.menu_opcion_padre',
          'rol.usuario.empleado.tipo_servicio',
          'rol.usuario.cliente'
        ]
    })
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'tipo_rol no existe' } });
      
      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Tipo_rol.forge(conditions).fetch({
    withRelated: [
      'usuario.rol.menu_opcion.menu_opcion_padre.menu_opcion_padre',
      'menu_opcion.menu_opcion_padre.menu_opcion_padre'
    ]
  })
    .then(function(tipo_rol){
      if(!tipo_rol) return res.status(404).json({ error : true, data : { message : 'tipo_rol no existe' } });

      let updateData = {
        descripcion:    req.body.descripcion,
      }
      
      tipo_rol.save(updateData)
        .then(function(data){
            res.status(200).json({ error : false, data : data.toJSON(), message : 'tipo_rol actualizado' });
        })
        .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : true, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Tipo_rol.forge(conditions).fetch()
    .then(function(tipo_rol){
      if(!tipo_rol) return res.status(404).json({ error : true, data : { message : 'tipo_rol no existe' } });

      Tipo_rol.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'tipo_rol eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}