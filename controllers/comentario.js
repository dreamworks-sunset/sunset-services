//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Comentario = require('../models/comentario');

exports.findDocuments = (req,res) => {
  
  Comentario.forge().fetchAll({withRelated:['respuesta_comentario','tipo_comentario','categoria_comentario','cliente']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {

  let newData = {
    id_cliente:         req.body.id_cliente,
    id_tipo_comentario:         req.body.tipo_comentario.id,
    contenido:    req.body.contenido,
    id_categoria_comentario:  req.body.categoria_comentario.id,
  }

  Comentario.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'comentario creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Comentario.forge(conditions).fetch({withRelated:['respuesta_comentario','tipo_comentario','categoria_comentario','cliente']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'servicio no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Comentario.forge(conditions).fetch()
    .then(function(servicio){
      if(!servicio) return res.status(404).json({ error : true, data : { message : 'servicio no existe' } });

      //encriptado de contraseña
      let salt = bcrypt.genSaltSync(12);
      let hash = bcrypt.hashSync(req.body.contrasenia, salt);

      let updateData = {
        id_rol:         req.body.id_rol,
        correo:         req.body.correo,
        contrasenia:    hash,
        ultimo_acceso:  req.body.ultimo_acceso,
        fecha_creacion: req.body.fecha_creacion,
        estatus:        req.body.estatus,
      }
      
      servicio.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'servicio actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Comentario.forge(conditions).fetch()
    .then(function(servicio){
      if(!servicio) return res.status(404).json({ error : true, data : { message : 'servicio no existe' } });

      servicio.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'servicio eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}