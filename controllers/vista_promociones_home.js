//----dependencias------  
'use strict'
const Vista_promociones_home = require('../models/vista_promociones_home');

exports.findDocuments = (req,res) => {
    
  Vista_promociones_home.forge().fetchAll()
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}