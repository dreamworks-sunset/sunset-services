//----dependencias------  
'use strict'
const bcrypt = require("bcryptjs");
const Insumo_utilizado = require('../models/insumo_utilizado');

exports.findDocuments = (req,res) => {
  
  Insumo_utilizado.forge().fetchAll({withRelated:['sesion','insumo_marca_proveedor','unidad']})
  .then(function(data){
    res.status(200).json({ error : false, data : data.toJSON() });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });
  });

}

exports.createDocument = (req,res) => {
  console.log(req.body);
  let newData = {
    id_sesion:                  req.body.id_sesion,
    id_insumo_marca_proveedor:  req.body.id_insumo_marca_proveedor,
    id_unidad:                  req.body.id_unidad
  }

  Insumo_utilizado.forge(newData).save()
  .then(function(data){
    res.status(200).json({ error: false, data: { message: 'insumo_utilizado creado' } });
  })
  .catch(function (err) {
    res.status(500).json({ error: true, data: {message: err.message} });

  });

}

exports.findOneDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Insumo_utilizado.forge(conditions).fetch({withRelated:['sesion','insumo_marca_proveedor','unidad']})
    .then(function(data){
      if(!data) return res.status(404).json({ error : true, data : { message : 'insumo_utilizado no existe' } });

      res.status(200).json({ error : false, data : data.toJSON() })

    })
    .catch(function(err){
      res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.updateDocument = (req,res) => {
  let conditions = { id: req.params.id };

  Insumo_utilizado.forge(conditions).fetch()
    .then(function(insumo_utilizado){
      if(!insumo_utilizado) return res.status(404).json({ error : true, data : { message : 'insumo_utilizado no existe' } });

      let updateData = {
        id_sesion:                  req.body.id_sesion,
        id_insumo_marca_proveedor:  req.body.id_insumo_marca_proveedor,
        id_unidad:                  req.body.id_unidad,
        estatus:                    req.body.estatus
      }
      
      insumo_utilizado.save(updateData)
        .then(function(data){
          res.status(200).json({ error : false, data : { message : 'insumo_utilizado actualizado'} });
        })
        .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} });
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}

exports.deleteDocument = (req,res) => {

  let conditions = { id: req.params.id };

  Insumo_utilizado.forge(conditions).fetch()
    .then(function(insumo_utilizado){
      if(!insumo_utilizado) return res.status(404).json({ error : true, data : { message : 'insumo_utilizado no existe' } });

      insumo_utilizado.destroy()
        .then(function(data){
          res.status(200).json({ error : false, data : {message : 'insumo_utilizado eliminado'} })
        })
        .catch(function(err){
          res.status(500).json({error : true, data : {message : err.message}});
        })

    })
    .catch(function(err){
          res.status(500).json({ error : false, data : {message : err.message} })
    })

}