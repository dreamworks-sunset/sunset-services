//----dependencias------ 
'use strict'
const express = require('express')

//----dependencias------ 
const router = express.Router()
const controller = require('../controllers/vista_reporte_tipo_comentario_por_rango_edad_y_sexo')

//----Parametros------
const path = '/vista_reporte_tipo_comentario_por_rango_edad_y_sexo'
const id = ':id'

//----Rutas------ 
router.get(`${path}`, controller.findDocuments)
router.get(`${path}/${id}`,controller.findOneDocument)

module.exports = router;