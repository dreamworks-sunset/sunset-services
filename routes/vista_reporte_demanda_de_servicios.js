//----dependencias------ 
'use strict'
const express = require('express')

//----dependencias------ 
const router = express.Router()
const controller = require('../controllers/vista_reporte_demanda_de_servicios')

//----Parametros------
const path = '/vista_reporte_demanda_de_servicios'

//----Rutas------ 
router.get(`${path}`, controller.findDocuments)

module.exports = router;