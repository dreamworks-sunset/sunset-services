//----dependencias------ 
'use strict'
const express = require('express')

//----dependencias------ 
const router = express.Router()
const controller = require('../controllers/servicios_con_su_demanda')

//----Parametros------
const path = '/servicios_con_su_demanda'

//----Rutas------ 
router.get(`${path}`, controller.findDocuments)

module.exports = router;
