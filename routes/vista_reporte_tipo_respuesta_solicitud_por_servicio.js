//----dependencias------ 
'use strict'
const express = require('express')

//----dependencias------ 
const router = express.Router()
const controller = require('../controllers/vista_reporte_tipo_respuesta_solicitud_por_servicio')

//----Parametros------
const path = '/vista_reporte_tipo_respuesta_solicitud_por_servicio'
const id = ':id'

//----Rutas------ 
router.get(`${path}`, controller.findDocuments)
router.get(`${path}/${id}`,controller.findOneDocument)

module.exports = router;