//----dependencias------ 
'use strict'
const express = require('express')

//----dependencias------ 
const router = express.Router()
const controller = require('../controllers/vista_parametros_gustos_clientes')

//----Parametros------
const path = '/vista_parametros_gustos_clientes'

//----Rutas------ 
router.get(`${path}`, controller.findDocuments)

module.exports = router;
