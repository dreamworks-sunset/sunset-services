//----dependencias------ 
'use strict'
const express = require('express')

//----dependencias------ 
const router = express.Router()
const controller = require('../controllers/vista_promociones_home')

//----Parametros------
const path = '/vista_promociones_home'

//----Rutas------ 
router.get(`${path}`, controller.findDocuments)

module.exports = router;
