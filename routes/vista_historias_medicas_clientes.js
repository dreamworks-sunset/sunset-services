//----dependencias------ 
'use strict'
const express = require('express')

//----dependencias------ 
const router = express.Router()
const controller = require('../controllers/vista_historias_medicas_clientes')

//----Parametros------
const path = '/vista_historias_medicas_clientes'

//----Rutas------ 
router.get(`${path}`, controller.findDocuments)

module.exports = router;
