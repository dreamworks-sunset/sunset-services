//----dependencias------ 
'use strict'
const express = require('express');
const auth = require('../middlewares/auth');

//----routes------
const agenda = require('./agenda');
const bloque_hora = require('./bloque_hora');
const categoria_comentario = require('./categoria_comentario');
const categoria_comentariomovil = require('./categoria_comentario');
const ciudad = require('./ciudad');
const cita = require('./cita');
const clasificacion_incidencia = require('./clasificacion_incidencia');
const clasificacion_parametro = require('./clasificacion_parametro');
const cliente = require('./cliente');
const clientemovil = require('./cliente');
const clientemovilimagen = require('./cliente');
const condicion_garantia = require('./condicion_garantia');
const comentario = require('./comentario');
const criterio = require('./criterio');
const dia_laborable = require('./dia_laborable');
const empleado = require('./empleado');
const empleado_tipo_servicio = require('./empleado_tipo_servicio');
const estado = require('./estado');
const evaluacion = require('./evaluacion');
const evaluacion_sesion = require('./evaluacion_sesion');
const eventualidad = require('./eventualidad');
const filosofia = require('./filosofia');
const garantia = require('./garantia');
const horario = require('./horario');
const horario_empleado = require('./horario_empleado');
const imagen = require('./imagen');
const incidencia = require('./incidencia');
const insumo = require('./insumo');
const insumo_marca_proveedor = require('./insumo_marca_proveedor');
const insumo_servicio = require('./insumo_servicio');
const insumo_utilizado = require('./insumo_utilizado');
const item_detalle = require('./item_detalle');
const item_logro = require('./item_logro');
const marca = require('./marca');
const menu_opcion = require('./menu_opcion');
const negocio = require('./negocio');
const notificacion = require('./notificacion');
const orden_servicio = require('./orden_servicio');
const pais = require('./pais');
const parametro = require('./parametro');
const perfil = require('./perfil');
const presupuesto = require('./presupuesto');
const pregunta_frecuente = require('./pregunta_frecuente');
const promocion = require('./promocion');
const proveedor = require('./proveedor');
const reclamo = require('./reclamo');
const red_social = require('./red_social');
const red_social_negocio = require('./red_social_negocio');
const respuesta_comentario = require('./respuesta_comentario');
const respuesta_reclamo = require('./respuesta_reclamo');
const respuesta_solicitud = require('./respuesta_solicitud');
const rol = require('./rol');
const seccion_web = require('./seccion_web');
const servicio = require('./servicio');
const servicios_con_su_demanda = require('./servicios_con_su_demanda');
const sesion = require('./sesion');
const sistema= require('./sistema');
const solicitud = require('./solicitud');
const suscripcion = require('./suscripcion');
const tecnica = require('./tecnica');
const tipo_avance = require('./tipo_avance');
const tipo_comentario = require('./tipo_comentario');
const tipo_comentariomovil = require('./tipo_comentario');
const tipo_incidencia = require('./tipo_incidencia');
const tipo_motivo = require('./tipo_motivo');
const tipo_notificacion = require('./tipo_notificacion');
const tipo_parametro = require('./tipo_parametro');
const tipo_reclamo = require('./tipo_reclamo');
const tipo_respuesta_comentario = require('./tipo_respuesta_comentario');
const tipo_respuesta_presupuesto = require('./tipo_respuesta_presupuesto');
const tipo_respuesta_reclamo = require('./tipo_respuesta_reclamo');
const tipo_respuesta_solicitud = require('./tipo_respuesta_solicitud');
const tipo_rol = require('./tipo_rol');
const tipo_servicio = require('./tipo_servicio');
const tipo_solicitud = require('./tipo_solicitud');
const tipo_tecnica = require('./tipo_tecnica');
const unidad = require('./unidad');
const usuario = require('./usuario');
const valor_parametro = require('./valor_parametro');
const vista_avance_todos = require('./vista_avance_todos');
const vista_citas_del_dia= require('./vista_citas_del_dia');
const vista_citas_no_atendidas = require('./vista_citas_no_atendidas');
const vista_dashboard1 = require('./vista_dashboard1');
const vista_gustos_clientes= require('./vista_gustos_clientes');
const vista_historias_medicas_clientes= require('./vista_historias_medicas_clientes');
const vista_parametros_gustos_clientes= require('./vista_parametros_gustos_clientes');
const vista_parametros_historias_medicas_clientes= require('./vista_parametros_historias_medicas_clientes');
const vista_parametros_preferencias_clientes= require('./vista_parametros_preferencias_clientes');
const vista_preferencias_clientes= require('./vista_preferencias_clientes');
const vista_promociones_home = require('./vista_promociones_home');
const vista_reporte_demanda_de_servicios = require('./vista_reporte_demanda_de_servicios');
const vista_reporte_estructurado_post_servicio = require('./vista_reporte_estructurado_post_servicio');
const vista_reporte_estructurado_solicitud_servicio = require('./vista_reporte_estructurado_solicitud_servicio');
const vista_reporte_gestion_servicio = require('./vista_reporte_gestion_servicio');
const vista_reporte_nivel_confianza = require('./vista_reporte_nivel_confianza');
const vista_reporte_reclamos_por_servicio = require('./vista_reporte_reclamos_por_servicio');
const vista_reporte_tipo_comentario_por_rango_edad_y_sexo = require('./vista_reporte_tipo_comentario_por_rango_edad_y_sexo');
const vista_reporte_tipo_respuesta_reclamo_por_servicio = require('./vista_reporte_tipo_respuesta_reclamo_por_servicio');
const vista_reporte_tipo_respuesta_solicitud_por_servicio = require('./vista_reporte_tipo_respuesta_solicitud_por_servicio');


//----app------
const app = express();

//---- Rutas Publicas ------
app.use('/',
	agenda,
	bloque_hora,
	categoria_comentario,
	categoria_comentariomovil,//Ruta para movil
	ciudad,
	cita,
	clasificacion_incidencia,
	clasificacion_parametro,
	condicion_garantia,
	cliente,
	clientemovil,
	clientemovilimagen,
	comentario,
	criterio,
	dia_laborable,
	empleado,
	empleado_tipo_servicio,
	estado,
	evaluacion,
	evaluacion_sesion,
	eventualidad,
	filosofia,
	garantia,
	horario,
	horario_empleado,
	imagen,
	incidencia,
	insumo,
	insumo_marca_proveedor,
	insumo_servicio,
	insumo_utilizado,
	item_detalle,
	item_logro,
	marca,
	menu_opcion,
	negocio,
	notificacion,
	orden_servicio,
	pais,
	parametro,
	perfil,
	presupuesto,
	pregunta_frecuente,
	promocion,
	proveedor,
	reclamo,
	red_social,
	red_social_negocio,
	respuesta_comentario,
	respuesta_reclamo,
	respuesta_solicitud,
	rol,
	seccion_web,
	servicio,
	servicios_con_su_demanda,
	sesion,
	sistema,
	solicitud,
	suscripcion,
	tecnica,
	tipo_avance,
	tipo_comentario,
	tipo_comentariomovil,//Ruta para movil
	tipo_incidencia,
	tipo_motivo,
	tipo_notificacion,
	tipo_parametro,
	tipo_reclamo,
	tipo_respuesta_comentario,
	tipo_respuesta_presupuesto,
	tipo_respuesta_reclamo,
	tipo_respuesta_solicitud,
	tipo_rol,
	tipo_servicio,
	tipo_solicitud,
	tipo_tecnica,
	unidad,
	usuario,
	valor_parametro,
	vista_avance_todos,
	vista_citas_no_atendidas,
	vista_dashboard1,
	vista_citas_del_dia,
	vista_gustos_clientes,
	vista_historias_medicas_clientes,
	vista_parametros_historias_medicas_clientes,
	vista_parametros_preferencias_clientes,
	vista_parametros_gustos_clientes,
	vista_preferencias_clientes,
	vista_promociones_home,
	vista_reporte_demanda_de_servicios,
	vista_reporte_estructurado_post_servicio,
	vista_reporte_estructurado_solicitud_servicio,
	vista_reporte_gestion_servicio,
	vista_reporte_nivel_confianza,
	vista_reporte_reclamos_por_servicio,
	vista_reporte_tipo_comentario_por_rango_edad_y_sexo,
	vista_reporte_tipo_respuesta_reclamo_por_servicio,
	vista_reporte_tipo_respuesta_solicitud_por_servicio

);

//---- Rutas Privadas ------
app.use('/',auth
	//usuario
);

//----Exportar Rutas------ 
module.exports = app
