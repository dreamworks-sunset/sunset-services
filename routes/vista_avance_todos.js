//----dependencias------ 
'use strict'
const express = require('express')

//----dependencias------ 
const router = express.Router()
const controller = require('../controllers/vista_avance_todos')

//----Parametros------
const path = '/vista_avance_todos'

//----Rutas------ 
router.get(`${path}`, controller.findDocuments)

module.exports = router;
