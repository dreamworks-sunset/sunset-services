//----dependencias------ 
'use strict'
const express = require('express')

//----dependencias------ 
const router = express.Router()
const controller = require('../controllers/vista_reporte_tipo_respuesta_reclamo_por_servicio')

//----Parametros------
const path = '/vista_reporte_tipo_respuesta_reclamo_por_servicio'

//----Rutas------ 
router.get(`${path}`, controller.findDocuments)

module.exports = router;