//---- dependencias ------
const Bookshelf = require('../db');

const Insumo_marca_proveedor = Bookshelf.Model.extend({
  tableName: 'insumo_marca_proveedor',
  insumo: function() {
    return this.belongsTo('Insumo','id_insumo');
  },
  marca: function() {
    return this.belongsTo('Marca','id_marca');
  },
  proveedor: function() {
    return this.belongsTo('Proveedor','id_proveedor');
  },
  sesion: function() {
    return this.belongsToMany('Sesion','insumo_utilizado','id_insumo_marca_proveedor','id_sesion');
  },
  insumo_utilizado: function() {
    return this.hasMany('Insumo_utilizado','id_insumo_marca_proveedor');
  },
  incidencia: function() {
    return this.belongsToMany('Incidencia','incidencia_insumo','id_insumo_marca_proveedor','id_incidencia');
  },
});

module.exports = Bookshelf.model('Insumo_marca_proveedor',Insumo_marca_proveedor);