//---- dependencias ------
const Bookshelf = require('../db');

const Vista_reporte_reclamos_por_servicio = Bookshelf.Model.extend({
  tableName: 'vista_reporte_reclamos_por_servicio'
});

module.exports = Bookshelf.model('Vista_reporte_reclamos_por_servicio', Vista_reporte_reclamos_por_servicio);