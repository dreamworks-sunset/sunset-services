//---- dependencias ------
const Bookshelf = require('../db');

const Vista_parametros_preferencias_clientes = Bookshelf.Model.extend({
  tableName: 'vista_parametros_preferencias_clientes'
});

module.exports = Bookshelf.model('Vista_parametros_preferencias_clientes', Vista_parametros_preferencias_clientes);