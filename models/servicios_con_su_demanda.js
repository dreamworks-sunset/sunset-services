//---- dependencias ------
const Bookshelf = require('../db');

const Servicios_con_su_demanda = Bookshelf.Model.extend({
  tableName: 'servicios_con_su_demanda'
});

module.exports = Bookshelf.model('Servicios_con_su_demanda', Servicios_con_su_demanda);