//---- dependencias ------
const Bookshelf = require('../db');

const Evaluacion_sesion = Bookshelf.Model.extend({
  tableName: 'evaluacion_sesion',
  evaluacion: function() {
    return this.belongsTo('Evaluacion', 'id_evaluacion');
  },
  criterio: function() {
    return this.belongsTo('Criterio', 'id_criterio');
  },
  sesion: function() {
    return this.belongsTo('Sesion', 'id_sesion');
  }
});

module.exports = Bookshelf.model('Evaluacion_sesion', Evaluacion_sesion);