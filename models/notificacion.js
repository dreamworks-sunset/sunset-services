//---- dependencias ------
const Bookshelf = require('../db');
const Solicitud = require('./solicitud');
const Presupuesto = require('./presupuesto');
const Respuesta_presupuesto = require('./respuesta_presupuesto');
const Reclamo = require('./reclamo');
const Respuesta_reclamo = require('./respuesta_reclamo');
const Comentario = require('./comentario');

const Notificacion = Bookshelf.Model.extend({
  tableName: 'notificacion',
  tipo_notificacion: function() {
    return this.belongsTo('Tipo_notificacion','id_tipo_notificacion');
  },
  usuario: function() {
    return this.belongsTo('Usuario','id_usuario');
  },
});

module.exports = Bookshelf.model('Notificacion', Notificacion);