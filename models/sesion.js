//---- dependencias ------
const Bookshelf = require('../db');

const Sesion = Bookshelf.Model.extend({
  tableName: 'sesion',
  cita: function() {
    return this.belongsTo('Cita','id_cita');
  },
  incidencia: function() {
      return this.hasOne('Incidencia','id_sesion');
  },
  evaluacion: function() {
    return this.belongsToMany('Evaluacion','evaluacion_sesion','id_sesion','id_evaluacion');
  },
  criterio: function() {
    return this.belongsToMany('Criterio','evaluacion_sesion', 'id_sesion','id_criterio');
  },
  insumo: function() {
    return this.belongsToMany('Insumo_marca_proveedor','insumo_utilizado','id_sesion','id_insumo_marca_proveedor');
  },
  insumo_utilizado: function() {
    return this.hasMany('Insumo_utilizado','id_sesion');
  },
  tecnica: function() {
    return this.belongsToMany('Tecnica','tecnica_sesion','id_sesion','id_tecnica');
  },
  item_logro: function() {
    return this.belongsToMany('Item_logro','detalle_sesion','id_sesion','id_item_logro');
  },
  item_detalle: function() {
    return this.belongsToMany('Item_detalle','detalle_sesion','id_sesion','id_item_detalle');
  },
  valor_parametro: function() {
    return this.belongsToMany('Valor_parametro','avance','id_sesion','id_valor_parametro');
  },
});

module.exports = Bookshelf.model('Sesion', Sesion);