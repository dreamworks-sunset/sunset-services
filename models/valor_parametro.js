//---- dependencias ------
const Bookshelf = require('../db');

const Valor_parametro = Bookshelf.Model.extend({
  tableName: 'valor_parametro',
  parametro: function() {
    return this.belongsTo('Parametro','id_parametro');
  },
  unidad: function() {
    return this.belongsTo('Unidad','id_unidad');
  },
  clasificacion_parametro: function() {
    return this.belongsTo('Clasificacion_parametro','id_clasificacion_parametro');
  },
  cliente: function() {
    return this.belongsToMany('Cliente', 'perfil', 'id_valor_parametro', 'id_cliente');
  },
  perfil: function(){
     return this.hasMany('Perfil', 'id_valor_parametro');
  },
  servicio: function() {
    return this.belongsToMany('Servicio', 'valor_parametro_servicio', 'id_valor_parametro', 'id_servicio');
  },
  servicio_avance: function() {
    return this.belongsToMany('Servicio', 'valor_parametro_avance', 'id_valor_parametro', 'id_servicio');
  },
  promocion: function() {
    return this.belongsToMany('Promocion', 'detalle_promocion', 'id_valor_parametro', 'id_promocion');
  },
  sesion: function() {
    return this.belongsToMany('Sesion', 'avance', 'id_valor_parametro', 'id_sesion');
  }
});

module.exports = Bookshelf.model('Valor_parametro', Valor_parametro);