//---- dependencias ------
const Bookshelf = require('../db');

const Criterio = Bookshelf.Model.extend({
  tableName: 'criterio',
  sesion: function() {
    return this.belongsToMany('Sesion','evaluacion_sesion', 'id_criterio','id_sesion');
  },
  evaluacion: function() {
    return this.belongsToMany('Evaluacion','evaluacion_sesion', 'id_criterio','id_evaluacion');
  },
});

module.exports = Bookshelf.model('Criterio', Criterio);