//---- dependencias ------
const Bookshelf = require('../db');

const Red_social = Bookshelf.Model.extend({
  tableName: 'red_social',
  negocio: function() {
    return this.belongsToMany('Negocio','red_social_negocio','id_red_social','id_negocio');
  },
  red_social_negocio: function() {
    return this.hasMany('Red_social_negocio','id_red_social');
  }
 
});

module.exports = Bookshelf.model('Red_social', Red_social);