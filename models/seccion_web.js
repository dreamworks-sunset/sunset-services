//---- dependencias ------
const Bookshelf = require('../db');

const Seccion_web = Bookshelf.Model.extend({
  
    tableName: 'seccion_web',
    
  imagen: function() {
    return this.belongsToMany('Imagen','seccion_web_imagen','id_seccion_web','id_imagen');
  },
});

module.exports = Bookshelf.model('Seccion_web', Seccion_web);