//---- dependencias ------
const Bookshelf = require('../db');

const Tecnica = Bookshelf.Model.extend({
  tableName: 'tecnica',
  tipo_tecnica: function() {
    return this.belongsTo('Tipo_tecnica', 'id_tipo_tecnica');
  },
  servicio: function() {
    return this.belongsToMany('Servicio','tecnica_servicio','id_tecnica','id_servicio');
  },
  sesion: function() {
    return this.belongsToMany('Sesion','tecnica_sesion','id_tecnica','id_sesion');
  },
});

module.exports = Bookshelf.model('Tecnica', Tecnica);