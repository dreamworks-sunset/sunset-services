//---- dependencias ------
const Bookshelf = require('../db');

const Insumo_servicio = Bookshelf.Model.extend({
  tableName: 'insumo_servicio',
  servicio: function() {
    return this.belongsTo('Servicio','id_servicio');
  },
  insumo: function() {
    return this.belongsTo('Insumo','id_insumo');
  },
  unidad: function() {
    return this.belongsTo('Unidad','id_unidad');
  },
});

module.exports = Bookshelf.model('Insumo_servicio',Insumo_servicio);