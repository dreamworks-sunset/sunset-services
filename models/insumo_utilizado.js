//---- dependencias ------
const Bookshelf = require('../db');

const Insumo_utilizado = Bookshelf.Model.extend({
  tableName: 'insumo_utilizado',
  sesion: function() {
    return this.belongsTo('Sesion','id_sesion');
  },
  insumo_marca_proveedor: function() {
    return this.belongsTo('Insumo_marca_proveedor','id_insumo_marca_proveedor');
  },
  unidad: function() {
    return this.belongsTo('Unidad','id_unidad');
  },
});

module.exports = Bookshelf.model('Insumo_utilizado',Insumo_utilizado);