//---- dependencias ------
const Bookshelf = require('../db');

const Servicio = Bookshelf.Model.extend({
  tableName: 'servicio',
  tipo_servicio: function() {
    return this.belongsTo('Tipo_servicio','id_tipo_servicio');
  },
  solicitud: function() {
    return this.hasMany('Solicitud','id_servicio');
  },
  promocion: function() {
    return this.hasMany('Promocion','id_servicio');
  },
  valor_parametro_servicio: function() {
    return this.belongsToMany('Valor_parametro','valor_parametro_servicio','id_servicio','id_valor_parametro');
  },
  valor_parametro_avance: function() {
    return this.belongsToMany('Valor_parametro','valor_parametro_avance','id_servicio','id_valor_parametro');
  },
  tecnica: function() {
    return this.belongsToMany('Tecnica','tecnica_servicio','id_servicio','id_tecnica');
  },
  condicion_garantia: function() {
    return this.belongsToMany('Condicion_garantia','condicion_servicio','id_servicio','id_condicion_garantia');
  },
  insumo: function() {
    return this.belongsToMany('Insumo','insumo_servicio','id_servicio','id_insumo');
  },
  insumo_servicio: function() {
    return this.hasMany('Insumo_servicio','id_servicio');
  },

});

module.exports = Bookshelf.model('Servicio', Servicio);