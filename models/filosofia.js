//---- dependencias ------
const Bookshelf = require('../db');

const Filosofia = Bookshelf.Model.extend({
  tableName: 'filosofia',
  negocio: function() {
    return this.belongsTo('Negocio','id_negocio');
  }
});


module.exports = Bookshelf.model('Filosofia', Filosofia);