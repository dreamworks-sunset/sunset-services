//---- dependencias ------
const Bookshelf = require('../db');

const Vista_citas_del_dia = Bookshelf.Model.extend({
  tableName: 'vista_citas_del_dia'
});

module.exports = Bookshelf.model('Vista_citas_del_dia', Vista_citas_del_dia);