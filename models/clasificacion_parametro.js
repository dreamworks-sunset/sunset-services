//---- dependencias ------
const Bookshelf = require('../db');

const Clasificacion_parametro = Bookshelf.Model.extend({
  tableName: 'clasificacion_parametro',
  valor_parametro: function() {
    return this.hasMany('Valor_parametro','id_clasificacion_parametro');
  },
});

module.exports = Bookshelf.model('Clasificacion_parametro', Clasificacion_parametro);