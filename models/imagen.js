//---- dependencias ------
const Bookshelf = require('../db');

const Imagen = Bookshelf.Model.extend({
  tableName: 'imagen',

  sistema: function() {
    return this.belongsTo('Sistema','id_sistema');
  },
  seccion_web: function() {
    return this.belongsToMany('Seccion_web','seccion_web_imagen','id_seccion_web', 'id_imagen');
  },
  promocion: function() {
    return this.hasOne('Promocion','id_imagen');
  },

});

module.exports = Bookshelf.model('Imagen', Imagen);