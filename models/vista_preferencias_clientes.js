//---- dependencias ------
const Bookshelf = require('../db');

const Vista_preferencias_clientes = Bookshelf.Model.extend({
  tableName: 'vista_preferencias_clientes'
});

module.exports = Bookshelf.model('Vista_preferencias_clientes', Vista_preferencias_clientes);