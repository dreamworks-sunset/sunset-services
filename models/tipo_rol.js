//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_rol = Bookshelf.Model.extend({
  tableName: 'tipo_rol',
  rol: function() {
    return this.hasMany('Rol','id_tipo_rol');
  },
});

module.exports = Bookshelf.model('Tipo_rol', Tipo_rol);