//---- dependencias ------
const Bookshelf = require('../db');

const Vista_reporte_estructurado_solicitud_servicio = Bookshelf.Model.extend({
  tableName: 'vista_reporte_estructurado_solicitud_servicio'
});

module.exports = Bookshelf.model('Vista_reporte_estructurado_solicitud_servicio', Vista_reporte_estructurado_solicitud_servicio);