//---- dependencias ------
const Bookshelf = require('../db');

const Respuesta_presupuesto = Bookshelf.Model.extend({
  tableName: 'respuesta_presupuesto',
  tipo_respuesta_presupuesto: function() {
    return this.belongsTo('Tipo_respuesta_presupuesto','id_tipo_respuesta_presupuesto');
  },
  presupuesto: function() {
    return this.belongsTo('Presupuesto','id_presupuesto');
  },
});

module.exports = Bookshelf.model('Respuesta_presupuesto', Respuesta_presupuesto);