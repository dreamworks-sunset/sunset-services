//---- dependencias ------
const Bookshelf = require('../db');

const Empleado = Bookshelf.Model.extend({
  tableName: 'empleado',
  solicitud: function() {
    return this.hasMany('Solicitud','id_empleado');
  },
  negocio: function() {
    return this.belongsTo('Negocio','id_negocio');
  },
  usuario: function() {
    return this.belongsTo('Usuario','id_usuario');
  },
  ciudad: function() {
    return this.belongsTo('Ciudad','id_ciudad');
  },
  tipo_servicio: function() {
    return this.belongsToMany('Tipo_servicio', 'empleado_tipo_servicio','id_empleado','id_tipo_servicio');
  },
  horario: function() {
    return this.belongsToMany('Horario', 'horario_empleado','id_empleado','id_horario');
  },
   horario_empleado: function() {
    return this.hasMany('Horario_empleado','id_empleado');
  },

});


module.exports = Bookshelf.model('Empleado', Empleado);