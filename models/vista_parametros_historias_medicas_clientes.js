//---- dependencias ------
const Bookshelf = require('../db');

const Vista_parametros_historias_medicas_clientes = Bookshelf.Model.extend({
  tableName: 'vista_parametros_historias_medicas_clientes'
});

module.exports = Bookshelf.model('Vista_parametros_historias_medicas_clientes', Vista_parametros_historias_medicas_clientes);