//---- dependencias ------
const Bookshelf = require('../db');

const Comentario = Bookshelf.Model.extend({
  tableName: 'comentario',
  categoria_comentario: function() {
    return this.belongsTo('Categoria_comentario', 'id_categoria_comentario');
  },
  tipo_comentario: function() {
    return this.belongsTo('Tipo_comentario', 'id_tipo_comentario');
  },
  respuesta_comentario: function() {
    return this.hasOne('Respuesta_comentario', 'id_comentario');
  },
  cliente: function() {
    return this.belongsTo('Cliente', 'id_cliente');
  },
});

module.exports = Bookshelf.model('Comentario', Comentario);