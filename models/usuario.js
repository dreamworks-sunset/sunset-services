//---- dependencias ------
const Bookshelf = require('../db');

const Usuario = Bookshelf.Model.extend({
  tableName: 'usuario',
  rol: function() {
    return this.belongsTo('Rol','id_rol');
  },
  cliente: function() {
    return this.hasOne('Cliente','id_usuario');
  },
  empleado: function() {
    return this.hasOne('Empleado','id_usuario');
  },
  notificacion: function() {
    return this.hasMany('Notificacion','id_usuario');
  },
});

module.exports = Bookshelf.model('Usuario', Usuario);