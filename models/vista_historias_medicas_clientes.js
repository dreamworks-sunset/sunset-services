//---- dependencias ------
const Bookshelf = require('../db');

const Vista_historias_medicas_clientes = Bookshelf.Model.extend({
  tableName: 'vista_historias_medicas_clientes'
});

module.exports = Bookshelf.model('Vista_historias_medicas_clientes', Vista_historias_medicas_clientes);