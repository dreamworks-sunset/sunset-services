//---- dependencias ------
const Bookshelf = require('../db');

const Vista_reporte_nivel_confianza = Bookshelf.Model.extend({
  tableName: 'vista_reporte_nivel_confianza'
});

module.exports = Bookshelf.model('Vista_reporte_nivel_confianza', Vista_reporte_nivel_confianza);