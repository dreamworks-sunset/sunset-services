//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_tecnica = Bookshelf.Model.extend({
  tableName: 'tipo_tecnica',
  tecnica: function() {
    return this.hasMany('Tecnica', 'id_tipo_tecnica');
  },
});

module.exports = Bookshelf.model('Tipo_tecnica', Tipo_tecnica);