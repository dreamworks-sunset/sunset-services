//---- dependencias ------
const Bookshelf = require('../db');

const Vista_citas_no_atendidas = Bookshelf.Model.extend({
  tableName: 'vista_citas_no_atendidas'
});

module.exports = Bookshelf.model('Vista_citas_no_atendidas', Vista_citas_no_atendidas);