//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_respuesta_reclamo = Bookshelf.Model.extend({
  tableName: 'tipo_respuesta_reclamo',
  respuesta_reclamo: function() {
    return this.hasMany('Respuesta_reclamo', 'id_tipo_respuesta_reclamo');
  }
});

module.exports = Bookshelf.model('Tipo_respuesta_reclamo', Tipo_respuesta_reclamo);