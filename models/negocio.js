//---- dependencias ------
const Bookshelf = require('../db');

const Negocio = Bookshelf.Model.extend({
  tableName: 'negocio',
  empleado: function() {
    return this.hasMany('Empleado','id_negocio');
  },
  filosofia: function() {
    return this.hasMany('Filosofia','id_negocio');
  },
   pregunta_frecuente: function() {
    return this.hasMany('Pregunta_frecuente','id_negocio');
  },
  red_social: function() {
    return this.belongsToMany('Red_social','red_social_negocio','id_negocio','id_red_social');
  },
  red_social_negocio: function() {
    return this.hasMany('Red_social_negocio','id_negocio');
  }

});


module.exports = Bookshelf.model('Negocio', Negocio);