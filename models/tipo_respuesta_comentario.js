//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_respuesta_comentario = Bookshelf.Model.extend({
  tableName: 'tipo_respuesta_comentario',
  respuesta_comentario: function() {
    return this.hasMany('Respuesta_comentario', 'id_tipo_respuesta_comentario');
  }
});

module.exports = Bookshelf.model('Tipo_respuesta_comentario', Tipo_respuesta_comentario);