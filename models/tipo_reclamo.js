//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_reclamo= Bookshelf.Model.extend({
  tableName: 'tipo_reclamo',
  reclamo: function() {
    return this.hasMany('Reclamo', 'id_tipo_reclamo');
  }
});

module.exports = Bookshelf.model('Tipo_reclamo', Tipo_reclamo);