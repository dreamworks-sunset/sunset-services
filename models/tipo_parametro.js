//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_parametro = Bookshelf.Model.extend({
  tableName: 'tipo_parametro',
  parametro: function() {
    return this.hasMany('Parametro','id_tipo_parametro');
  },
});

module.exports = Bookshelf.model('Tipo_parametro', Tipo_parametro);