const Bookshelf = require('../db');


const Condicion_garantia = Bookshelf.Model.extend({
  tableName: 'condicion_garantia',

    servicio: function() {
    return this.belongsToMany('Servicio', 'condicion_servicio', 'id_condicion_garantia', 'id_servicio');
  }
});

module.exports = Bookshelf.model('Condicion_garantia', Condicion_garantia);