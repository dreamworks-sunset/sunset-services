//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_notificacion = Bookshelf.Model.extend({
  tableName: 'tipo_notificacion',
  notificacion: function() {
    return this.hasMany('Notificacion','id_tipo_notificacion');
  }
});

module.exports = Bookshelf.model('Tipo_notificacion', Tipo_notificacion);