//---- dependencias ------
const Bookshelf = require('../db');

const Rol = Bookshelf.Model.extend({
  tableName: 'rol',
  usuario: function() {
    return this.hasMany('Usuario','id_rol');
  },
  menu_opcion: function() {
    return this.belongsToMany('Menu_opcion', 'opcion_rol', 'id_rol', 'id_menu_opcion');
  },
  tipo_rol: function() {
    return this.belongsTo('Tipo_rol', 'id_tipo_rol');
  }
});

module.exports = Bookshelf.model('Rol', Rol);