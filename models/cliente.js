'use strict'

//---- dependencias ------
const Bookshelf = require('../db');

const Cliente = Bookshelf.Model.extend({
  tableName: 'cliente',
  idAttribute: 'id',
  usuario: function() {
    return this.belongsTo('Usuario', 'id_usuario');
  },
  valor_parametro: function() {
    return this.belongsToMany('Valor_parametro', 'perfil', 'id_cliente', 'id_valor_parametro');
  },
  perfil: function() {
    return this.hasMany('Perfil', 'id_cliente');
  },
  solicitud: function() {
    return this.hasMany('Solicitud','id_cliente');
  },
});

module.exports = Bookshelf.model('Cliente', Cliente);
