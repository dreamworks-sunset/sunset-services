//---- dependencias ------
const Bookshelf = require('../db');

const Perfil = Bookshelf.Model.extend({
  tableName: 'perfil',
  idAttribute: 'id',
  valor_parametro: function() {
    return this.belongsTo('Valor_parametro', 'id_valor_parametro');
  },
  cliente: function() {
    return this.belongsTo('Cliente', 'id_cliente');
  },
});

module.exports = Bookshelf.model('Perfil', Perfil);
