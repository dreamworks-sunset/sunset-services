//---- dependencias ------
const Bookshelf = require('../db');

const Horario_empleado = Bookshelf.Model.extend({
  tableName: 'horario_empleado',
  empleado: function() {
    return this.belongsTo('Empleado','id_empleado');
  },
  horario: function() {
    return this.belongsTo('Horario','id_horario');
  },
  agenda: function() {
    return this.hasMany('Agenda','id_horario_empleado');
  },
});


module.exports = Bookshelf.model('Horario_empleado', Horario_empleado);