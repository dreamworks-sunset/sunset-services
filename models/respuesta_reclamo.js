//---- dependencias ------
const Bookshelf = require('../db');

const Respuesta_reclamo = Bookshelf.Model.extend({
  tableName: 'respuesta_reclamo',
  tipo_respuesta_reclamo: function() {
    return this.belongsTo('Tipo_respuesta_reclamo','id_tipo_respuesta_reclamo');
  },
  reclamo: function() {
    return this.belongsTo('Reclamo','id_reclamo');
  },
});

module.exports = Bookshelf.model('Respuesta_reclamo', Respuesta_reclamo);