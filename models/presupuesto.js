//---- dependencias ------
const Bookshelf = require('../db');

const Presupuesto = Bookshelf.Model.extend({
  tableName: 'presupuesto',
  solicitud: function() {
    return this.belongsTo('Solicitud','id_solicitud');
  },
  respuesta_presupuesto: function() {
    return this.hasOne('Respuesta_presupuesto','id_presupuesto');
  },
});

module.exports = Bookshelf.model('Presupuesto', Presupuesto);