//---- dependencias ------
const Bookshelf = require('../db');

const Cita = Bookshelf.Model.extend({
  tableName: 'cita',
  orden_servicio: function() {
    return this.belongsTo('Orden_servicio','id_orden_servicio');
  },
  sesion: function() {
    return this.hasOne('Sesion','id_cita');
  },
  agenda: function() {
    return this.hasOne('Agenda','id_cita');
  },
});

module.exports = Bookshelf.model('Cita', Cita);