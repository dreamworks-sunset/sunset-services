//---- dependencias ------
const Bookshelf = require('../db');

const Garantia = Bookshelf.Model.extend({
  tableName: 'garantia',
  orden_servicio: function() {
    return this.belongsTo('Orden_servicio','id_orden_servicio');
  },
});

module.exports = Bookshelf.model('Garantia', Garantia);