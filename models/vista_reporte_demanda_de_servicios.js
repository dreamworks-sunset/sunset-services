//---- dependencias ------
const Bookshelf = require('../db');

const Vista_reporte_demanda_de_servicios = Bookshelf.Model.extend({
  tableName: 'vista_reporte_demanda_de_servicios'
});

module.exports = Bookshelf.model('vista_reporte_demanda_de_servicios', Vista_reporte_demanda_de_servicios);