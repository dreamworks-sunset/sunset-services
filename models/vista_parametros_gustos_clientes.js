//---- dependencias ------
const Bookshelf = require('../db');

const Vista_parametros_gustos_clientes = Bookshelf.Model.extend({
  tableName: 'vista_parametros_gustos_clientes'
});

module.exports = Bookshelf.model('Vista_parametros_gustos_clientes', Vista_parametros_gustos_clientes);