//---- dependencias ------
const Bookshelf = require('../db');

const Vista_gustos_clientes = Bookshelf.Model.extend({
  tableName: 'vista_gustos_clientes'
});

module.exports = Bookshelf.model('Vista_gustos_clientes', Vista_gustos_clientes);