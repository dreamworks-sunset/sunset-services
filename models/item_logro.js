//---- dependencias ------
const Bookshelf = require('../db');

const Item_logro = Bookshelf.Model.extend({
  tableName: 'item_logro',
  sesion: function() {
    return this.belongsToMany('Sesion','detalle_sesion','id_item_logro','id_sesion');
  },
  item_detalle: function() {
    return this.belongsToMany('Item_detalle','detalle_sesion','id_item_detalle','id_sesion');
  },
});

module.exports = Bookshelf.model('Item_logro',Item_logro);