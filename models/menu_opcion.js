const Bookshelf = require('../db');

const Menu_opcion = Bookshelf.Model.extend({
  tableName: 'menu_opcion',
  menu_opcion_padre: function() {
    return this.belongsTo('Menu_opcion','id_menu_opcion');
  },
  menu_opcion: function() {
    return this.hasMany('Menu_opcion','id_menu_opcion');
  },
  rol: function() {
    return this.belongsToMany('Rol', 'opcion_rol', 'id_menu_opcion', 'id_rol');
  }
});

module.exports = Bookshelf.model('Menu_opcion', Menu_opcion);