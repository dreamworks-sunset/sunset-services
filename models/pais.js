//---- dependencias ------
const Bookshelf = require('../db');

const Pais = Bookshelf.Model.extend({
  tableName: 'pais',
   estado: function() {
    return this.hasMany('Estado','id_pais');
  }
});


module.exports = Bookshelf.model('Pais', Pais);