//---- dependencias ------
const Bookshelf = require('../db');

const Clasificacion_incidencia = Bookshelf.Model.extend({
  tableName: 'clasificacion_incidencia',
  incidencia: function() {
    return this.hasMany('Incidencia','id_clasificacion_incidencia');
  },
});

module.exports = Bookshelf.model('Clasificacion_incidencia', Clasificacion_incidencia);