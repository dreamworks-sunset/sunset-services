//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_incidencia = Bookshelf.Model.extend({
  tableName: 'tipo_incidencia',
  incidencia: function() {
    return this.hasMany('Incidencia', 'id_tipo_incidencia');
  }
});

module.exports = Bookshelf.model('Tipo_incidencia', Tipo_incidencia);