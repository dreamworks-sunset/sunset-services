//---- dependencias ------
const Bookshelf = require('../db');

const Parametro = Bookshelf.Model.extend({
  tableName: 'parametro',
  tipo_parametro: function() {
    return this.belongsTo('Tipo_parametro','id_tipo_parametro');
  },
  valor_parametro: function() {
    return this.hasMany('Valor_parametro','id_parametro');
  },
});

module.exports = Bookshelf.model('Parametro', Parametro);