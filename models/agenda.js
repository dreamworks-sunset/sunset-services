//---- dependencias ------
const Bookshelf = require('../db');

const Agenda = Bookshelf.Model.extend({
  tableName: 'agenda',
  cita: function() {
    return this.belongsTo('Cita','id_cita');
  },
  horario_empleado: function() {
    return this.belongsTo('Horario_empleado','id_horario_empleado');
  },
  

});

module.exports = Bookshelf.model('Agenda', Agenda);