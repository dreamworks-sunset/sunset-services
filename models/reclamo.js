const Bookshelf = require('../db');

const Reclamo = Bookshelf.Model.extend({
    tableName: 'reclamo',
    tipo_reclamo: function() {
        return this.belongsTo('Tipo_reclamo','id_tipo_reclamo');
    },
    orden_servicio: function() {
        return this.belongsTo('Orden_servicio','id_orden_servicio');
    },
    respuesta_reclamo: function() {
        return this.hasOne('Respuesta_reclamo','id_reclamo');
    },
    cliente: function() {
    return this.belongsTo('Cliente', 'id_cliente');
  },
});

module.exports = Bookshelf.model('Reclamo', Reclamo);