//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_motivo = Bookshelf.Model.extend({
  tableName: 'tipo_motivo',
  solicitud: function() {
    return this.hasMany('Solicitud', 'id_tipo_motivo');
  }
});

module.exports = Bookshelf.model('Tipo_motivo', Tipo_motivo);