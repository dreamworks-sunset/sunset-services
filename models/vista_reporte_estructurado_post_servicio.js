//---- dependencias ------
const Bookshelf = require('../db');

const Vista_reporte_estructurado_post_servicio = Bookshelf.Model.extend({
  tableName: 'vista_reporte_estructurado_post_servicio'
});

module.exports = Bookshelf.model('Vista_reporte_estructurado_post_servicio', Vista_reporte_estructurado_post_servicio);