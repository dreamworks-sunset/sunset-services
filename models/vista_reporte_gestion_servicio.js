//---- dependencias ------
const Bookshelf = require('../db');

const Vista_reporte_gestion_servicio = Bookshelf.Model.extend({
  tableName: 'vista_reporte_gestion_servicio'
});

module.exports = Bookshelf.model('Vista_reporte_gestion_servicio', Vista_reporte_gestion_servicio);