//---- dependencias ------
const Bookshelf = require('../db');

const Horario = Bookshelf.Model.extend({
  tableName: 'horario',
  dia_laborable: function() {
    return this.belongsTo('Dia_laborable','id_dia_laborable');
  },
  bloque_hora: function() {
    return this.belongsTo('Bloque_hora','id_bloque_hora');
  },
  empleado: function() {
    return this.belongsToMany('Empleado','horario_empleado','id_horario','id_empleado');
  }
});

module.exports = Bookshelf.model('Horario', Horario);