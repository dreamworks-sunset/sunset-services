//---- dependencias ------
const Bookshelf = require('../db');

const Incidencia = Bookshelf.Model.extend({
  tableName: 'incidencia',
  tipo_incidencia: function() {
    return this.belongsTo('Tipo_incidencia','id_tipo_incidencia');
  },
  clasificacion_incidencia: function() {
    return this.belongsTo('Clasificacion_incidencia','id_clasificacion_incidencia');
  },
  sesion: function() {
    return this.belongsTo('Sesion','id_sesion');
  },
  insumo: function (){
  	return this.belongsToMany('Insumo_marca_proveedor','incidencia_insumo','id_incidencia','id_insumo_marca_proveedor');
  }
});

module.exports = Bookshelf.model('Incidencia', Incidencia);