//---- dependencias ------
const Bookshelf = require('../db');

const Vista_reporte_tipo_comentario_por_rango_edad_y_sexo = Bookshelf.Model.extend({
  tableName: 'vista_reporte_tipo_comentario_por_rango_edad_y_sexo'
});

module.exports = Bookshelf.model('Vista_reporte_tipo_comentario_por_rango_edad_y_sexo', Vista_reporte_tipo_comentario_por_rango_edad_y_sexo);