//---- dependencias ------
const Bookshelf = require('../db');

const Respuesta_solicitud = Bookshelf.Model.extend({
  tableName: 'respuesta_solicitud',
  tipo_respuesta_solicitud: function() {
    return this.belongsTo('Tipo_respuesta_solicitud','id_tipo_respuesta_solicitud');
  },
  solicitud: function() {
    return this.belongsTo('Solicitud','id_solicitud');
  },
});

module.exports = Bookshelf.model('Respuesta_solicitud', Respuesta_solicitud);