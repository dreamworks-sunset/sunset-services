//---- dependencias ------
const Bookshelf = require('../db');

const Bloque_hora = Bookshelf.Model.extend({
  tableName: 'bloque_hora',
  horario: function() {
    return this.hasMany('Horario','id_bloque_hora');
  },
  solicitud: function() {
    return this.hasMany('Solicitud','id_bloque_hora');
  },
});

module.exports = Bookshelf.model('Bloque_hora', Bloque_hora);