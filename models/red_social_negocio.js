//---- dependencias ------
const Bookshelf = require('../db');

const Red_social_negocio = Bookshelf.Model.extend({
  tableName: 'red_social_negocio',
  
  negocio: function() {
    return this.belongsTo('Negocio','id_negocio');
  },
  
  red_social: function() {
    return this.belongsTo('Red_social','id_red_social');
  }
});


module.exports = Bookshelf.model('Red_social_negocio', Red_social_negocio);