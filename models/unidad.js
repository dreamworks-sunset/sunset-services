//---- dependencias ------
const Bookshelf = require('../db');

const Unidad = Bookshelf.Model.extend({
  tableName: 'unidad',
  valor_parametro: function() {
    return this.hasMany('Valor_parametro','id_unidad');
  },
  insumo: function() {
    return this.belongsToMany('Insumo','insumo_servicio','id_unidad','id_insumo');
  },
  insumo_utilizado: function() {
    return this.hasMany('Insumo_utilizado','id_unidad');
  },
});

module.exports = Bookshelf.model('Unidad', Unidad);