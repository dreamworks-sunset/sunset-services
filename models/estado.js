//---- dependencias ------
const Bookshelf = require('../db');

const Estado = Bookshelf.Model.extend({
  tableName: 'estado',
    ciudad: function() {
    return this.hasMany('Ciudad','id_estado');
  },
  pais: function() {
    return this.belongsTo('Pais','id_pais');
  }

});


module.exports = Bookshelf.model('Estado', Estado);