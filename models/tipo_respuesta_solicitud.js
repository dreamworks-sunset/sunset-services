//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_respuesta_solicitud = Bookshelf.Model.extend({
  tableName: 'tipo_respuesta_solicitud',
  respuesta_solicitud: function() {
    return this.hasMany('Respuesta_solicitud', 'id_tipo_respuesta_solicitud');
  }
});

module.exports = Bookshelf.model('Tipo_respuesta_solicitud', Tipo_respuesta_solicitud);