//---- dependencias ------
const Bookshelf = require('../db');

const Proveedor = Bookshelf.Model.extend({
  tableName: 'proveedor',
  ciudad: function() {
    return this.belongsTo('Ciudad','id_ciudad');
  },  
  insumo: function() {
    return this.belongsToMany('Insumo','insumo_marca_proveedor','id_proveedor','id_insumo');
  },
  marca: function() {
    return this.belongsToMany('Marca','insumo_marca_proveedor','id_proveedor','id_marca');
  },
});


module.exports = Bookshelf.model('Proveedor', Proveedor);