//---- dependencias ------
const Bookshelf = require('../db');

const Ciudad = Bookshelf.Model.extend({
  tableName: 'ciudad',
  estado: function() {
    return this.belongsTo('Estado','id_estado');
  },
  negocio: function() {
    return this.hasOne('Negocio','id_ciudad');
  },
});


module.exports = Bookshelf.model('Ciudad', Ciudad);