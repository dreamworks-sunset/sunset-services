//---- dependencias ------
const Bookshelf = require('../db');

const Insumo = Bookshelf.Model.extend({
  tableName: 'insumo',
  marca: function() {
    return this.belongsToMany('Marca','insumo_marca_proveedor','id_insumo','id_marca');
  },
  servicio: function() {
    return this.belongsToMany('Servicio','insumo_servicio','id_insumo','id_servicio');
  },
  insumo_servicio: function() {
    return this.hasMany('Insumo_servicio','id_insumo');
  },
  proveedor: function() {
    return this.belongsToMany('Proveedor','insumo_marca_proveedor','id_insumo','id_proveedor');
  },
  insumo_marca_proveedor: function() {
    return this.hasMany('Insumo_marca_proveedor','id_insumo');
  },
});

module.exports = Bookshelf.model('Insumo',Insumo);