//---- dependencias ------
const Bookshelf = require('../db');

const Orden_servicio = Bookshelf.Model.extend({
  tableName: 'orden_servicio',
  solicitud: function() {
    return this.belongsTo('Solicitud','id_solicitud');
  },
  orden_servicio_garantia: function() {
    return this.belongsTo('Orden_servicio','id_orden_servicio_garantia');
  },
  orden_servicio: function() {
    return this.hasOne('Orden_servicio','id_orden_servicio_garantia');
  },
  cita: function() {
    return this.hasMany('Cita','id_orden_servicio');
  },
  reclamo: function() {
    return this.hasMany('Reclamo','id_orden_servicio');
  },
  garantia: function() {
    return this.hasOne('Garantia','id_orden_servicio');
  },
});

module.exports = Bookshelf.model('Orden_servicio', Orden_servicio);