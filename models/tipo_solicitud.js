//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_solicitud = Bookshelf.Model.extend({
  tableName: 'tipo_solicitud',
  solicitud: function() {
    return this.hasMany('Solicitud', 'id_tipo_solicitud');
  }
});

module.exports = Bookshelf.model('Tipo_solicitud', Tipo_solicitud);