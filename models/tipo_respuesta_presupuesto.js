//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_respuesta_presupuesto = Bookshelf.Model.extend({
  tableName: 'tipo_respuesta_presupuesto',
  respuesta_presupuesto: function() {
    return this.hasMany('Respuesta_presupuesto', 'id_tipo_respuesta_presupuesto');
  }
});

module.exports = Bookshelf.model('Tipo_respuesta_presupuesto', Tipo_respuesta_presupuesto);