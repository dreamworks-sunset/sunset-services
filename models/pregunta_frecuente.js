//---- dependencias ------
const Bookshelf = require('../db');
require('./pregunta_frecuente');   //nombre de la table
/*const Servicio = Bookshelf.Model.extend({
  tableName: 'servicio',
});*/

const Pregunta_frecuente = Bookshelf.Model.extend({  //nombre del de la variable q guarda recult
  tableName: 'pregunta_frecuente',
  negocio: function() {					//tabla relacionada negocio
    return this.belongsTo('Negocio','id_negocio');  //id de la tabla relacionada
  }
});

module.exports = Bookshelf.model('Pregunta_frecuente', Pregunta_frecuente); //nombre del obj q guarda la el resultado d la func