//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_comentario = Bookshelf.Model.extend({
  tableName: 'tipo_comentario',
  comentario: function() {
    return this.hasMany('Comentario', 'id_tipo_comentario');
  }
});

module.exports = Bookshelf.model('Tipo_comentario', Tipo_comentario);