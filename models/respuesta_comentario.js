//---- dependencias ------
const Bookshelf = require('../db');

const Respuesta_comentario = Bookshelf.Model.extend({
  tableName: 'respuesta_comentario',
  tipo_respuesta_comentario: function() {
    return this.belongsTo('Tipo_respuesta_comentario','id_tipo_respuesta_comentario');
  },
  comentario: function() {
    return this.belongsTo('Comentario','id_comentario');
  },
});

module.exports = Bookshelf.model('Respuesta_comentario', Respuesta_comentario);