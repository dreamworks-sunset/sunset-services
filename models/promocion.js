const Bookshelf = require('../db');

const Promocion = Bookshelf.Model.extend({
  tableName: 'promocion',
    servicio: function() {
 return this.belongsTo('Servicio','id_servicio');
  },
  solicitud: function() {
    return this.hasMany('Solicitud','id_promocion');
  },
  detalle_promocion: function() {
    return this.belongsToMany('Valor_parametro','detalle_promocion','id_promocion','id_valor_parametro');
  }

});

module.exports = Bookshelf.model('Promocion', Promocion);