//---- dependencias ------
const Bookshelf = require('../db');

const Solicitud = Bookshelf.Model.extend({
  tableName: 'solicitud',
  cliente: function() {
    return this.belongsTo('Cliente','id_cliente');
  },
  servicio: function() {
    return this.belongsTo('Servicio','id_servicio');
  },
  promocion: function() {
    return this.belongsTo('Promocion','id_promocion');
  },
  tipo_solicitud: function() {
    return this.belongsTo('Tipo_solicitud','id_tipo_solicitud');
  },
  empleado: function() {
    return this.belongsTo('Empleado','id_empleado');
  },
  bloque_hora: function() {
    return this.belongsTo('Bloque_hora','id_bloque_hora');
  },
  tipo_motivo: function() {
    return this.belongsTo('Tipo_motivo','id_tipo_motivo');
  },
  respuesta_solicitud: function() {
    return this.hasOne('Respuesta_solicitud','id_solicitud');
  },
  presupuesto: function() {
    return this.hasOne('Presupuesto','id_solicitud');
  },
  orden_servicio: function() {
    return this.hasOne('Orden_servicio','id_solicitud');
  },
});

module.exports = Bookshelf.model('Solicitud', Solicitud);