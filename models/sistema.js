//---- dependencias ------
const Bookshelf = require('../db');

const sistema = Bookshelf.Model.extend({
  tableName: 'sistema',
  negocio: function() {
    return this.hasOne('Negocio','id_sistema');
  },


});

module.exports = Bookshelf.model('sistema', sistema);