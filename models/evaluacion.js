//---- dependencias ------
const Bookshelf = require('../db');

const Evaluacion = Bookshelf.Model.extend({
  tableName: 'evaluacion',
  sesion: function() {
    return this.belongsToMany('Sesion','evaluacion_sesion', 'id_evaluacion','id_sesion');
  },
  criterio: function() {
    return this.belongsToMany('Criterio','evaluacion_sesion', 'id_evaluacion','id_criterio');
  },
});

module.exports = Bookshelf.model('Evaluacion', Evaluacion);