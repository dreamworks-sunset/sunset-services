//---- dependencias ------
const Bookshelf = require('../db');

const Eventualidad = Bookshelf.Model.extend({
  tableName: 'eventualidad',

  negocio: function() {
    return this.belongsTo('Negocio','id_negocio');
  }
});

module.exports = Bookshelf.model('Eventualidad', Eventualidad);