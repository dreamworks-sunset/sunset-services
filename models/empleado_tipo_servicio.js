//---- dependencias ------
const Bookshelf = require('../db');

const Empleado_tipo_servicio = Bookshelf.Model.extend({
  tableName: 'empleado_tipo_servicio',
});


module.exports = Bookshelf.model('Empleado_tipo_servicio', Empleado_tipo_servicio);