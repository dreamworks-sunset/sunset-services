//---- dependencias ------
const Bookshelf = require('../db');

const Item_detalle = Bookshelf.Model.extend({
  tableName: 'item_detalle',
  sesion: function() {
    return this.belongsToMany('Sesion','detalle_sesion','id_item_detalle','id_sesion');
  },
  item_logro: function() {
    return this.belongsToMany('Item_logro','detalle_sesion','id_item_detalle','id_item_logro');
  },
});

module.exports = Bookshelf.model('Item_detalle',Item_detalle);