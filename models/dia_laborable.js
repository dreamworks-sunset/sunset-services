//---- dependencias ------
const Bookshelf = require('../db');

const Dia_laborable = Bookshelf.Model.extend({
  tableName: 'dia_laborable',
  horario: function() {
    return this.hasMany('Horario','id_dia_laborable');
  }
 
});

module.exports = Bookshelf.model('Dia_laborable', Dia_laborable);