//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_servicio = Bookshelf.Model.extend({
    tableName: 'tipo_servicio',
    servicio: function() {
        return this.hasMany('Servicio', 'id_tipo_servicio');
    },
    empleado: function() {
        return this.belongsToMany('Empleado', 'empleado_tipo_servicio','id_tipo_servicio','id_empleado');
    },
});

module.exports = Bookshelf.model('Tipo_servicio', Tipo_servicio);