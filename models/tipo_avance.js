//---- dependencias ------
const Bookshelf = require('../db');

const Tipo_avance = Bookshelf.Model.extend({
  tableName: 'tipo_avance',
  avance: function() {
    return this.hasMany('Avance','id_tipo_avance');
  },
});

module.exports = Bookshelf.model('Tipo_avance',Tipo_avance);