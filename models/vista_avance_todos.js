//---- dependencias ------
const Bookshelf = require('../db');

const Vista_avance_todos = Bookshelf.Model.extend({
  tableName: 'vista_avance_todos'
});

module.exports = Bookshelf.model('Vista_avance_todos', Vista_avance_todos);