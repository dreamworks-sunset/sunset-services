//---- dependencias ------
const Bookshelf = require('../db');

const Categoria_comentario = Bookshelf.Model.extend({
  tableName: 'categoria_comentario',
  comentario: function() {
    return this.hasMany('Comentario','id_categoria_comentario');
  },
});

module.exports = Bookshelf.model('Categoria_comentario', Categoria_comentario);