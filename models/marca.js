//---- dependencias ------
const Bookshelf = require('../db');

const Marca = Bookshelf.Model.extend({
  tableName: 'marca',
  insumo: function() {
    return this.belongsToMany('Insumo','insumo_marca_proveedor','id_marca','id_insumo');
  },
  proveedor: function() {
    return this.belongsToMany('Proveedor','insumo_marca_proveedor','id_marca','id_proveedor');
  },
});

module.exports = Bookshelf.model('Marca',Marca);