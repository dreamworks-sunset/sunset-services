//---- dependencias ------
const Bookshelf = require('../db');

const Vista_promociones_home = Bookshelf.Model.extend({
  tableName: 'vista_promociones_home'
});

module.exports = Bookshelf.model('Vista_promociones_home', Vista_promociones_home);